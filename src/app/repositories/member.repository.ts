import { IMemberChangePermissionModel, IMemberListFilter, IMemberListModel, INewProjectMember } from "../models/member.models";
import { Permission } from '../models/project.models';
import Project from "../schemes/project.schema";
import { CryptService } from '../services/crypt.service';
import { ExceptionService } from "../services/exception.service";



export const Update = async (changePermissionModel: IMemberChangePermissionModel, memberId: string, userId: string)  => {

    const {
        projectId,
        permission
    } = changePermissionModel;

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndexPermission = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const userPermission = projectBD.users[userIndexPermission];

    if(!userPermission || userPermission.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para editar o membro');
    }

    const userIndex = projectBD.users.findIndex(e => e.user == memberId);

    if(userIndex === -1)
    {
        throw ExceptionService.GenerateException(400, 'Membro não encontrado');
    }
    
    if(projectBD.users[userIndex].user.toString() === projectBD.createdBy.toString()) {
        throw ExceptionService.GenerateException(400, 'O membro é o dono do projeto');
    }

    projectBD.users[userIndex].permission = permission;

    await Project.findByIdAndUpdate(projectId, { 
        users: [...projectBD.users]
    }, { new: true });

    return { 
        memberId,
        projectId,
        permission
    };
}

export const List = async (filter: IMemberListFilter, userId: string) => {

    const projectBD = await Project.findOne({_id: filter.projectId}).populate(['users.user']);

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    let members:IMemberListModel[] = [];

    projectBD.users.forEach(item => {
        if(!filter.name || item.user.name.includes(filter.name)) {
            members.push({
                _id: item.user._id,
                name: item.user.name,
                email: item.user.email,
                permission: Permission.MANAGER == item.permission ? Permission.MANAGER : Permission.DEFAULT,
                isOwner: projectBD.createdBy.toString() === item.user._id.toString()
            });
        }
    });

    return members;
}

export const Delete = async (id: string, projectId: string, userId: string) => {

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    if(id === projectBD.createdBy.toString()) {
        throw ExceptionService.GenerateException(400, 'O membro é o dono do projeto');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para deletar o membro');
    }

    const users = projectBD.users.filter(e => e.user != id);

    await Project.findByIdAndUpdate(projectId, { 
        users: [...users]
    }, { new: true });
}

export const GenerateLink = async (id: string, userId: string) => {

    const projectBD = await Project.findOne({_id: id});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para compartilhar o projeto');
    }

    const now = new Date();
    now.setHours(now.getHours() + 1);

    const data = JSON.stringify({
        projectId: id,
        validity: now.toJSON()
    });

    return CryptService.Encrypt(data);
    
}

export const Ingress = async (code: string, userId: string) => {

    const data = CryptService.Decrypt(code);

    if(!data) {
        throw ExceptionService.GenerateException(400, 'Link inválido');
    }

    const newProjectMember = JSON.parse(data) as INewProjectMember;

    const now = new Date();
    const validity = new Date(newProjectMember.validity);

    if(now > validity) {
        throw ExceptionService.GenerateException(400, 'A validade do link expirou');
    }

    const projectBD = await Project.findOne({_id: newProjectMember.projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex(e => e.user == userId);

    if(userIndex > -1)
    {
        throw ExceptionService.GenerateException(400, 'Você ja é membro deste projeto');
    }

    projectBD.users.push({
        user: userId,
        permission: Permission.DEFAULT,
        isFavorite: false
    });

    await Project.findByIdAndUpdate(newProjectMember.projectId, { 
        users: [...projectBD.users]
    }, { new: true });


    return { 
        projectId: projectBD._id,
        name: projectBD.name,
        permission: Permission.DEFAULT
    };
    
}