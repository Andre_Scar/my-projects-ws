import { serve } from "../../src/server";
import request from "supertest";
import Project from "../../src/app/schemes/project.schema";
import { Permission } from "../../src/app/models/project.models";
import { commonAuthHeaders } from "../utils";


export class ApiProject {

    static Create = async (name: string, adminUserId: string, defaultUserId: string) => {

        return await Project.create({ 
            name,
            countActivities: 0,
            countDone: 0,
            countDoing: 0,
            countVerify: 0,
            users: [
                {
                    user: adminUserId,
                    permission: Permission.MANAGER,
                    isFavorite: false
                },
                {
                    user: defaultUserId,
                    permission: Permission.DEFAULT,
                    isFavorite: false
                }
            ],
            createdBy: adminUserId
        });

    }

    static Delete = async (id: string) => {

        return await Project.findByIdAndDelete(id);

    }

    static FindById = async (id: string) => {

        return await Project.findById(id);

    }


    static GenerateShareLink = async (id: string, tokenAdmin: string) => {

        const response = await request(serve)
            .get(`/member/v1/generate-link/${id}`)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { text } = response;
        return text;
        
    }
}