import mongoose from "../database";
import { IProjectEntity } from "./project.entity";
import { IUserEntity } from "./user.entity";


export interface IActivityTypeEntity extends mongoose.Document { 
    name: string;
    description?: string;
    project?: IProjectEntity["_id"];
    createdAt?: Date;
    createdBy: IUserEntity["_id"];
};