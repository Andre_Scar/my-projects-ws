import * as activityTypeRepository  from '../repositories/activityType.repository';
import { IRequest, IResponse } from '../types/express';


export const Save = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityTypeRepository.Create(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Update = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityTypeRepository.Update(request.body, request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const FindById = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityTypeRepository.FindById(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const List = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityTypeRepository.List(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const ListAll = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityTypeRepository.ListAll(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Delete = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityTypeRepository.Delete(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}
