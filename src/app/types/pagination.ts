

export interface IPaginationQuery {
    pageSize: number;
    page: number;
    filter: any;
}


export interface IPageResult<T> {
    count: number;
    page: number;
    pageSize: number;
    results: T;
}