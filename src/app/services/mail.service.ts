import nodemailer from "nodemailer";
import path from "path";
import { ExceptionService } from "./exception.service";

var hbs = require('nodemailer-express-handlebars');

export interface IMailOptions {
    from: string;
    to: string;
    subject: string;
    template: string;
    context?: any;
}

export class MailService {

    private static transporter = nodemailer.createTransport({
        host: process.env.MAIL_HOST || 'smtp-mail.outlook.com',
        port: parseInt(process.env.MAIL_PORT || '587'),
        secure: false,
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASSWORD
        },
        tls: { rejectUnauthorized: false }
    });

    private static hbsConfig = {
        viewEngine: {
            extName: '.hbs',
            partialsDir: path.join(__dirname, '../resources/mail/'),
            layoutsDir: path.join(__dirname, '../resources/mail/'),
            defaultLayout: ''
        },
        viewPath: path.join(__dirname, '../resources/mail/'),
        extName: '.hbs'
    }

    static async sendMail(mailOptions: IMailOptions) {

        this.transporter.use('compile', hbs(this.hbsConfig));

        await this.transporter.sendMail(mailOptions, (error, info) => { console.log(error || 'E-mail enviado'); });
        
    }

}