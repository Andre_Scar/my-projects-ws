import { serve } from "../../src/server";
import request from "supertest";
import { commonAuthHeaders } from "../../.jest/utils";
import { ActivityStatus } from "../../src/app/entities/activity.entity";
import { CustomFieldType } from "../../src/app/entities/activityCustomField.entity";
import { Priority } from "../../src/app/entities/story.entity";


export class ApiActivity {

    static Create = async (
        title: string, 
        responsibleId: string, 
        typeId: string, 
        projectId: string, 
        storyId: string, 
        tokenAdmin: string,
        status: ActivityStatus = ActivityStatus.TO_DO,
        priority: Priority = Priority.HIGH
    ) => {

        const activity = {
            title,
            responsibleId,
            typeId,
            status,
            priority,
            storyId,
            projectId,
            customFields: []
        };

        const response = await request(serve)
            .post(`/activity/v1`)
            .send(activity)
            .set(commonAuthHeaders(tokenAdmin));
        
        return response.body.activity;

    }

    static Type = async (name:string, projectId: string, tokenAdmin: string) => {

        const type = {
            name,
            projectId
        };

        const response = await request(serve)
            .post(`/activity-type/v1`)
            .send(type)
            .set(commonAuthHeaders(tokenAdmin));
        
        return response.body.activityType;
    }

    static CustomField = async (name:string, type: CustomFieldType, activityTypeId: string, projectId: string, tokenAdmin: string) => {

        const activityCustomField = {
            name,
            type,
            activityTypeId,
            projectId
        };

        const response = await request(serve)
            .post(`/activity-custom-field/v1`)
            .send(activityCustomField)
            .set(commonAuthHeaders(tokenAdmin));
        
        return response.body.activityCustomField;
    }
    
}