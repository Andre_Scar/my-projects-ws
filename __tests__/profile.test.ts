import request from "supertest";
import { ApiAuth } from "../.jest/api/auth.api";
import { users } from "../.jest/mocks/user.mock";
import { commonAuthHeaders, dropDatabase } from "../.jest/utils";
import { serve } from "../src/server";

const END_POINT: string = `/profile/v1`;

describe('Profile', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {

        await dropDatabase();
        await serve.close();

    });

    beforeEach(async () => {

        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();

    });
    //#endregion

    test('Update profile: Should pass', async () => {
    
        const response = await request(serve)
            .put(`${END_POINT}/`)
            .send({
                name: 'user 1',
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const {_id, name } = response.body;
        
        expect(_id.toString()).toBe(adminUserId);
        expect(name).toBe('user 1');
    
    });

    test('Update profile by feke user: Should fail', async () => {

        const fakeUser = 'a9e746d51200c621cec119685b443eac';
    
        const response = await request(serve)
            .put(`${END_POINT}/`)
            .send({
                name: 'user 1',
            })
            .set(commonAuthHeaders(fakeUser));
        
        const { error } = response.body;
        expect(error).toBe('Token inválido');
    
    });

    test('Update profile with name to null : Should fail', async () => {

        const response = await request(serve)
            .put(`${END_POINT}/`)
            .send({
                name: null,
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    
    });

    test('Find profile by id: Should pass', async () => {
    
        const response = await request(serve)
            .get(`${END_POINT}/`)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { name, email, countMyProjects, countSharedWithMe } = response.body;
        
        expect(name).toBe(users.admin.name);
        expect(email).toBe(users.admin.email);
        expect(countMyProjects).toBe(0);
        expect(countSharedWithMe).toBe(0);
    });
    
});