import jwt from 'jsonwebtoken';
import { IRequest, IResponse } from '../types/express';

export default (request: IRequest, response: IResponse, next: any) => {
    const authHeader = request.headers.authorization;

    if(!authHeader) {
        return response.status(401).send({ error: 'Nenhum token fornecido'});
    }

    const parts:string[] = authHeader.split(' ');

    if(!(parts.length === 2)) {
        return response.status(401).send({ error: 'Erro de token'});
    }
    
    const [ scheme, token ] = parts;

    if(!/^Bearer$/i.test(scheme)) {
        return response.status(401).send({ error: 'Token mal formatado'});
    }

    const authSecret = process.env.AUTH_SECRET || '';
    
    jwt.verify(token, authSecret, (err, decoded: any) => {
        
        if(err) {
            return response.status(401).send({ error: 'Token inválido'});
        }

        request.userId = decoded.id;

        return next();
    });
};