import bcrypt from 'bcrypt';
import crypto from 'crypto';
import { OAuth2Client } from 'google-auth-library';
import { IGoogleData, IResetPassword, IUserModel } from '../models/auth.models';
import User from '../schemes/user.schema';
import { AuthService } from '../services/auth.service';
import { ExceptionService } from '../services/exception.service';
import { MailService } from '../services/mail.service';


export const create = async (userModel: IUserModel)  => {

    const { email, name, password } = userModel;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    if(!email || email === '') {
        throw ExceptionService.GenerateException(400, 'O campo e-mail é obrigatório');
    }

    if(!password || password === '') {
        throw ExceptionService.GenerateException(400, 'O campo senha é obrigatório');
    }

    if(await User.findOne({ email })) {
        throw ExceptionService.GenerateException(400, 'Usuário já existe');
    }

    const userBD = await User.create({ 
        email, 
        name, 
        password 
    });

    userBD.password = '';

    return { 
        user: userBD, 
        token: AuthService.GenerateToken({ id: userBD.id }) 
    };

}

export const authenticate = async(userModel: IUserModel) => {
    const {email, password} = userModel;

    const userBD = await User.findOne({ email }).select('+password');
    
    if(!userBD) {
        throw ExceptionService.GenerateException(404, 'Usuário não encontrado');
    }

    if(!userBD.password || !await bcrypt.compare(password, userBD.password)) {
        throw ExceptionService.GenerateException(400, 'Senha inválida');
    }

    userBD.password = '';
    
    return{ 
        user: userBD,
        token: AuthService.GenerateToken({ id: userBD.id }) 
    };
}

export const forgotPassword = async(userModel: IUserModel) => {
    const { email } = userModel;

    const userBD: any = await User.findOne({ email });

    if(!userBD) {
        throw ExceptionService.GenerateException(400, 'Usuário não encontrado');
    }

    const token = crypto.randomBytes(20).toString('hex');

    const now = new Date();
    now.setHours(now.getHours() + 1);

    await User.findByIdAndUpdate(userBD.id, {
        '$set': {
            passwordResetToken: token,
            passwordResetExpires: now
        }
    });

    await MailService.sendMail({
        to: email,
        from: process.env.MAIL_USER || 'my-projects-v1@hotmail.com',
        subject: process.env.APP_NAME || 'My Projects',
        template: 'auth/forgot_password',
        context: { 
            token,
            appClientURL: process.env.APP_CLIENT_URL
        }
    });

}

export const resetPassword = async (user: IResetPassword) => {
    const { email, token, password } = user;

    const userBD: any = await User.findOne({ email })
        .select('+passwordResetToken passwordResetExpires');

    if(!userBD) {
        throw ExceptionService.GenerateException(400, 'Usuário não encontrado');
    }

    if(token !== userBD.passwordResetToken) {
        throw ExceptionService.GenerateException(400, 'Token inválido');
    }

    const now = new Date();
    if(now > userBD.passwordResetExpires) {
        throw ExceptionService.GenerateException(400, 'Token expirado, gere um novo');
    }

    userBD.password = password;

    await userBD.save();
}

//#region google
export const google = async (googleData: IGoogleData) => {

    const client = new OAuth2Client();

    const ticket = await client.verifyIdToken({
        idToken: googleData.tokenId
    });

    if(!ticket) {
        throw ExceptionService.GenerateException(400, 'Erro ao realizar login');
    }

    const payload = ticket.getPayload(); 
    
    if(!payload || !payload.name || !payload.email) {
        throw ExceptionService.GenerateException(400, 'Erro ao obter informações de usuário google');
    }

    const userBD = await User.findOne({ email: payload.email });

    if(userBD) {
        return { 
            user: userBD, 
            token: AuthService.GenerateToken({ id: userBD.id }) 
        };
    }

    const newUserBD = await User.create({ 
        email: payload.email, 
        name: payload.name,
    });

    return { 
        user: userBD, 
        token: AuthService.GenerateToken({ id: newUserBD.id }) 
    };
}
//#endregion