import express from 'express';
import authMiddleware from './../middlewares/auth';

import * as activityTypeController from '../controllers/activityType.controller';


const router = express.Router();

router.use(authMiddleware);

router.post('/', activityTypeController.Save);
router.put('/:id', activityTypeController.Update);
router.get('/:id', activityTypeController.FindById);
router.post('/list', activityTypeController.List);
router.post('/list-all', activityTypeController.ListAll);
router.delete('/:id', activityTypeController.Delete);


export default (app: any) => app.use('/activity-type/v1', router);