import request from "supertest";
import { ApiAuth } from "../.jest/api/auth.api";
import { ApiProject } from "../.jest/api/project.api";
import { ApiSprint } from "../.jest/api/sprint.api";
import { ApiStory } from "../.jest/api/story.api";
import { ApiActivity } from "../.jest/api/activity.api";
import { users } from "../.jest/mocks/user.mock";
import { addDays, commonAuthHeaders, dropDatabase } from "../.jest/utils";
import { serve } from "../src/server";

const END_POINT: string = `/sprint/v1`;

describe('Sprints', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {

        await dropDatabase();
        await serve.close();

    });

    beforeEach(async () => {

        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();

    });
    //#endregion

    test('Create sprint: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);

        const today = new Date();

        const sprint = {
            name: 'Sprint 1',
            startAt: today,
            endAt: addDays(today, 15),
            projectId: projectDB._id
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(sprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const {_id, name, startAt, endAt } = response.body.sprint;
        
        expect(_id).not.toBe(null);
        expect(name).toBe(sprint.name);
        expect(startAt).toBe(sprint.startAt.toJSON());
        expect(endAt).toBe(sprint.endAt.toJSON());
    
    });

    test('Create sprint no name: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);

        const today = new Date();

        const sprint = {
            name: null,
            startAt: today,
            endAt: addDays(today, 15),
            projectId: projectDB._id
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(sprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    
    });

    test('Create sprint no start date: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);

        const today = new Date();

        const sprint = {
            name: 'Sprint 1',
            startAt: null,
            endAt: addDays(today, 15),
            projectId: projectDB._id
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(sprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo data de início é obrigatório');
    
    });

    test('Create sprint no end date: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);

        const today = new Date();

        const sprint = {
            name: 'Sprint 1',
            startAt: today,
            endAt: null,
            projectId: projectDB._id
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(sprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo data de fim é obrigatório');
    
    });

    test('Create sprint in fake project: Should fail', async () => {
        
        const fakeProjectId = '5fa0af509db40713507b0767';

        const today = new Date();

        const sprint = {
            name: 'Sprint 1',
            startAt: today,
            endAt: addDays(today, 15),
            projectId: fakeProjectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(sprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    
    });

    test('Create sprint by default user: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);

        const today = new Date();

        const sprint = {
            name: 'Sprint 1',
            startAt: today,
            endAt: addDays(today, 15),
            projectId: projectDB._id
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(sprint)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para criar sprint');
    
    });

    test('Update sprint: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const today = new Date();
        const updateSprint = {
            name: 'Sprint 2',
            startAt: addDays(today, 15),
            endAt: addDays(today, 30),
            projectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${sprintId}`)
            .send(updateSprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const {_id, name, startAt, endAt} = response.body.sprint;
        
        expect(_id).toBe(sprintId);
        expect(name).toBe(updateSprint.name);
        expect(startAt).toBe(updateSprint.startAt.toJSON());
        expect(endAt).toBe(updateSprint.endAt.toJSON());
    });

    test('Update sprint no name: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const today = new Date();
        const updateSprint = {
            name: null,
            startAt: addDays(today, 15),
            endAt: addDays(today, 30),
            projectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${sprintId}`)
            .send(updateSprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    });

    test('Update sprint no start date: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const today = new Date();
        const updateSprint = {
            name: 'Sprint 2',
            startAt: null,
            endAt: addDays(today, 30),
            projectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${sprintId}`)
            .send(updateSprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo data de início é obrigatório');
    });

    test('Update sprint no end date: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const today = new Date();
        const updateSprint = {
            name: 'Sprint 2',
            startAt: addDays(today, 15),
            endAt: null,
            projectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${sprintId}`)
            .send(updateSprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo data de fim é obrigatório');
    });

    test('Update sprint in fake project: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const fakeProjectId = '5fa0af509db40713507b0767';
        const today = new Date();
        const updateSprint = {
            name: 'Sprint 2',
            startAt: addDays(today, 15),
            endAt: addDays(today, 30),
            projectId: fakeProjectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${sprintId}`)
            .send(updateSprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    });

    test('Update sprint by default user: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const today = new Date();
        const updateSprint = {
            name: 'Sprint 2',
            startAt: addDays(today, 15),
            endAt: addDays(today, 30),
            projectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${sprintId}`)
            .send(updateSprint)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para editar a sprint');
    });

    test('Find sprint by id: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';
        
        const response = await request(serve)
            .get(`${END_POINT}/${sprintId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { _id, name, startAt, endAt } = response.body;

        expect(_id).toBe(sprintId);
        expect(name).toBe('Sprint 1');
        expect(startAt).toBe(sprintDB.startAt);
        expect(endAt).toBe(sprintDB.endAt);
    });

    test('Find fake sprint by id: Should fail', async () => {

        const fakeSprintId = '5fa0af509db40713507b0767';

        const response = await request(serve)
            .get(`${END_POINT}/${fakeSprintId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Sprint não encontrada');
    });

    test('List sprints: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        for(var i = 0; i < 5; i++) {
            await ApiSprint.Create(`Sprint ${i}`, projectId, tokenAdmin);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({
                pageSize: 10,
                page: 0,
                filter: {project: projectId}
            })
            .set(commonAuthHeaders(tokenAdmin));

        const { count, results } = response.body;

        expect(count).toBe(5);
        expect(results.length).toBe(5);
        expect(results[0].name).toBe('Sprint 0');
        expect(results[1].name).toBe('Sprint 1');
        expect(results[2].name).toBe('Sprint 2');
        expect(results[3].name).toBe('Sprint 3');
        expect(results[4].name).toBe('Sprint 4');
    });

    test('List sprints filtered by name: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        for(var i = 0; i < 5; i++) {
            await ApiSprint.Create(`Sprint ${i}`, projectId, tokenAdmin);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({
                pageSize: 10,
                page: 0,
                filter: {
                    project: projectId,
                    name: {'$regex' : '1', '$options' : 'i'}
                }
            })
            .set(commonAuthHeaders(tokenAdmin));

        const { count, results } = response.body;

        expect(count).toBe(1);
        expect(results.length).toBe(1);
        expect(results[0].name).toBe('Sprint 1');

    });

    test('Dalete sprint by user admin: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${sprintId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { status } = response;
        expect(status).toBe(200);

    });

    test('Dalete fake sprint: Should fail', async () => {

        const fakeSprintId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${fakeSprintId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Sprint não encontrada');

    });

    test('Dalete sprint by user default: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${sprintId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para deletar a sprint');
        
    });

    test('Dalete sprint with project not found: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';
        
        await ApiProject.Delete(projectId);

        const response = await request(serve)
            .delete(`${END_POINT}/${sprintId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
        
    });

    test('Export csv by sprint: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';


        const response = await request(serve)
            .get(`${END_POINT}/export-csv/${sprintId}`)
            .set(commonAuthHeaders(tokenAdmin));
    
        
        const { status, text } = response;
        expect(status).toBe(200);
        expect(text.indexOf('Sprint 1')).not.toBe(-1);
    });

    test('Export csv by sprint with story: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        await ApiStory.Create('História 1', sprintId, projectId, tokenAdmin);

        const response = await request(serve)
            .get(`${END_POINT}/export-csv/${sprintId}`)
            .set(commonAuthHeaders(tokenAdmin));
    
        
        const { status, text } = response;
        expect(status).toBe(200);
        expect(text.indexOf('Sprint 1')).not.toBe(-1);
        expect(text.indexOf('História 1')).not.toBe(-1);
    });

    test('Export csv by sprint with story and activity: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('História 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';
        
        const activityTypeDB = await ApiActivity.Type('Tipo 1', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        await ApiActivity.Create('Atividade 1', adminUserId, activityTypeId, projectId, storyId, tokenAdmin);

        const response = await request(serve)
            .get(`${END_POINT}/export-csv/${sprintId}`)
            .set(commonAuthHeaders(tokenAdmin));
    
        
        const { status, text } = response;
        
        expect(status).toBe(200);
        expect(text.indexOf('Sprint 1')).not.toBe(-1);
        expect(text.indexOf('História 1')).not.toBe(-1);
        expect(text.indexOf('Atividade 1')).not.toBe(-1);
        expect(text.indexOf('Tipo 1')).not.toBe(-1);
    });
    
});