import { IListSprintModel, ISprintModel } from "../models/sprint.models";
import Sprint from "../schemes/sprint.schema";
import { ExceptionService } from "../services/exception.service";
import { IPaginationQuery } from "../types/pagination";
import { parse } from "json2csv";
import dayjs from "dayjs";
import Story from "../schemes/story.schema";
import Activity from "../schemes/activity.schema";
import { ActivityStatus } from "../entities/activity.entity";
import Project from "../schemes/project.schema";
import { Permission } from "../models/project.models";
import * as activityRepository  from './activity.repository';
import { Priority } from "../entities/story.entity";

export const Create = async (sprintModel: ISprintModel, userId: string)  => {

    const { 
        name,
        startAt,
        endAt,
        projectId
    } = sprintModel;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    if(!startAt) {
        throw ExceptionService.GenerateException(400, 'O campo data de início é obrigatório');
    }

    if(!endAt) {
        throw ExceptionService.GenerateException(400, 'O campo data de fim é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para criar sprint');
    }

    const sprintBD = await Sprint.create({ 
        name,
        startAt,
        endAt,
        project: projectId,
        createdBy: userId
    });

    return { sprint: sprintBD };
}

export const Update = async (sprintModel: ISprintModel, sprintId: string, userId: string)  => {

    const { 
        name,
        startAt,
        endAt,
        projectId
    } = sprintModel;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    if(!startAt) {
        throw ExceptionService.GenerateException(400, 'O campo data de início é obrigatório');
    }

    if(!endAt) {
        throw ExceptionService.GenerateException(400, 'O campo data de fim é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para editar a sprint');
    }

    const sprintBD = await Sprint.findByIdAndUpdate(sprintId, { 
        name,
        startAt,
        endAt,
    }, { new: true });

    return { sprint: sprintBD };
}

export const FindById = async (id: string, userId: string) => {

    const sprintBD = await Sprint.findById(id).limit(1);

    if(!sprintBD) {
        throw ExceptionService.GenerateException(400, 'Sprint não encontrada');
    }

    return { 
        _id: sprintBD._id,
        name: sprintBD.name,
        startAt: sprintBD.startAt,
        endAt: sprintBD.endAt
    };
}

export const List = async (query: IPaginationQuery, userId: string) => {

    if(query.filter === null || Object.keys(query.filter).length == 0) {
        return {
            page: query.page,
            pageSize: query.pageSize,
            results: [],
            count: 0
        };
    }

    const sprints = await Sprint
        .find(query.filter)
        .skip(query.page * query.pageSize)
        .limit(query.pageSize)
        .populate(['createdBy']);

    const count = await Sprint.find(query.filter).countDocuments();

    const results:IListSprintModel[] = sprints.map(sprint => {
        return {
            _id: sprint._id,
            name: sprint.name,
            startAt: sprint.startAt,
            endAt: sprint.endAt,
            createdByName: sprint.createdBy.name,
        }
    });

    return {
        page: query.page,
        pageSize: query.pageSize,
        results: results,
        count: count
    };
}

export const Delete = async (id: string, userId: string) => {
    
    const sprintBD = await Sprint.findOne({_id: id});

    if(!sprintBD) {
        throw ExceptionService.GenerateException(400, 'Sprint não encontrada');
    }

    const projectId = sprintBD.project.toString();
    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const user = projectBD.users.find((item) => item.user.toString() === userId);

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para deletar a sprint');
    }

    let storiesId:string[] = [];
    const stories = await Story.find({sprint: id});
    stories.forEach(story => {
        storiesId.push(story._id.toString());
        story.deleteOne();
    });

    const activities = await Activity.find({story: { $in: storiesId }});
    activities.forEach(async activity => {
        await activityRepository.decrementCountActivities(projectId, activity.status);
        activity.deleteOne();
    });


    sprintBD.deleteOne();
}

export const ExportCSV = async (id: string) => {

    const sprintBD = await Sprint.findOne({_id: id});

    if(!sprintBD) {
        throw ExceptionService.GenerateException(400, 'Sprint não encontrada');
    }

    let fileData:string = '';
    let sprint = [
        {
            'Nome': sprintBD.name,
            'Data de inicio': dayjs(sprintBD.startAt).format('DD/MM/YYYY'),
            'Data de Fim': dayjs(sprintBD.endAt).format('DD/MM/YYYY')
        }
    ];

    const fileName = `${sprintBD.name}.csv`;
    const options = {
        delimiter: ';'
    }; 

    fileData = parse(sprint, options);

    const storiesBD = await Story.find({sprint: id});

    if(!storiesBD) {
        return {
            fileName,
            fileData
        };
    }

    let storyById:any = {};
    storiesBD.forEach(e => {
        storyById[e._id] = {
            'Id': FormatId(e.count),
            'História': e.description,
            'Prioridade': LabelPriority(e.priority)
        }
    });

    const storiesId = Object.keys(storyById);
    const activities = await Activity.find({ story: { $in: storiesId }}).populate(['responsible', 'type']);

    let activitiesByStoryId:any = {};
    activities.forEach(e => {
        const storyId = e.story.toString();

        if(!activitiesByStoryId[storyId]) {
            activitiesByStoryId[storyId] = []
        }

        activitiesByStoryId[storyId].push({
            'Id': FormatId(e.count),
            'Título': e.title,
            'Responsável': e.responsible.name,
            'Tipo': e.type.name,
            'Status': LabelStatus(e.status),
            'Prioridade': LabelPriority(e.priority)
        });
    });

    storiesId.forEach( e => {
        fileData = `${fileData}\r\n\r\n\r\n${parse([storyById[e]], options)}`;

        if(activitiesByStoryId[e] != null && activitiesByStoryId[e].length > 0) {
            fileData = `${fileData}\r\n${parse(activitiesByStoryId[e], options)}`;
        } 
    });

    return {
        fileName,
        fileData
    };
}

//#region help
const FormatId = (id: number) => {
    return `#${id.toString().padStart(4, '0')}`;
}

const LabelPriority = (priority: Priority) => {
    switch(priority)
    {
        case Priority.URGENT:
            return 'Urgente';
        case Priority.HIGH:
            return 'Alto';
        case Priority.MEDIUM:
            return 'Médio';
        case Priority.LOW:
            return 'Baixo';
    }
}

const LabelStatus = (priority: ActivityStatus) => {
    switch(priority)
    {
        case ActivityStatus.TO_DO:
            return 'A Fazer';
        case ActivityStatus.IN_PROGRESS:
            return 'Em Progresso';
        case ActivityStatus.TO_VERIFY:
            return 'Verificar';
        case ActivityStatus.DONE:
            return 'Feito';
    }
}
//#endregion