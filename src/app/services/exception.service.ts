
export interface IException {
    code: number,
    error: string
}

export class ExceptionService {

    static GenerateException(code: number, error: string): IException {
        return {code, error};
    }

}