import * as authRepository  from '../repositories/auth.repository';
import { IRequest, IResponse } from '../types/express';

export const Create = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await authRepository.create(request.body));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const Authenticate = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await authRepository.authenticate(request.body));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const ForgotPassword = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await authRepository.forgotPassword(request.body));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const ResetPassword = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await authRepository.resetPassword(request.body));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const Google = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await authRepository.google(request.body));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}