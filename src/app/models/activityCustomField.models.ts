import { CustomFieldType } from "../entities/activityCustomField.entity";
import { IUserModel } from "./auth.models";


export interface IActivityCustomFieldModel {
    name: string;
    type: CustomFieldType;
    activityTypeId: string;
    projectId: string;
    createdAt?: Date;
    createdBy: IUserModel;
};

export interface IListActivityCustomFieldModel {
    _id: string;
    name: string;
    type: string;
    activityTypeName: string;
    createdByName: string;
}

export interface IFilterByActivityId {
    activityTypeId: string;
}