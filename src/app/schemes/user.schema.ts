import bcrypt from 'bcrypt';
import mongoose from '../database';
import { IUserEntity } from '../entities/user.entity';

export const UserSchema = new mongoose.Schema<IUserEntity>({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        select: false
    },
    passwordResetToken: {
        type: String,
        select: false,
    },
    passwordResetExpires: {
        type: Date,
        select: false,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

UserSchema.pre<IUserEntity>('save', async function(next) {
    if(this.password && this.password != '') {
        const hash = await bcrypt.hash(this.password, 10);
        this.password = hash;
    }

    next();
});

const User = mongoose.model<IUserEntity>('User', UserSchema);

export default User;