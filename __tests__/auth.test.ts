import request from "supertest";
import { serve } from "../src/server";
import { dropDatabase } from "../.jest/utils";
import { ApiAuth } from "../.jest/api/auth.api";
import User from "../src/app/schemes/user.schema";

const END_POINT: string = `/auth/v1`;

describe('User', () => {

    //#region setup
    afterAll(async () => {
        await dropDatabase();
        await serve.close();
    });

    beforeEach(async () => {
        await dropDatabase();
    });
    //#endregion

    test('Fake user authentication: Should fail', async () => {
        const fakeUser = {
            email: 'fakeUser@gmail.com',
            password: '123456'
        };

        const response = await request(serve)
            .post(`${END_POINT}/authenticate`)
            .send(fakeUser);
        
        const { error } = response.body;
        expect(error).toBe('Usuário não encontrado');
    });

    test('User authentication: Should pass', async () => {
        const newUser = {
            name: 'admin',
            email: 'andrescar14@gmail.com',
            password: '123456'
        };

        await ApiAuth.Create(newUser);

        const response = await request(serve)
            .post(`${END_POINT}/authenticate`)
            .send({
                email: newUser.email,
                password: newUser.password
            });
        
        const {user, token} = response.body;

        expect(token).not.toBe(null);
        expect(user).not.toBe(null);
        expect(user.name).toBe(newUser.name);
        expect(user.email).toBe(newUser.email);
        expect(user.password).toBe('');
        expect(user.createdAt).not.toBe(null);
    });

    test('Create account: Should pass', async () => {
        const newUser = {
            name: 'admin',
            email: 'andrescar14@gmail.com',
            password: '123456'
        };

        const response = await request(serve)
            .post(`${END_POINT}/register`)
            .send(newUser);
        
        const {user, token} = response.body;

        expect(token).not.toBe(null);
        expect(user).not.toBe(null);
        expect(user.name).toBe(newUser.name);
        expect(user.email).toBe(newUser.email);
        expect(user.password).toBe('');
        expect(user.createdAt).not.toBe(null);
    });

    test('Create unnamed account: Should fail', async () => {
        const newUser = {
            email: 'andrescar14@gmail.com',
            password: '123456'
        };

        const response = await request(serve)
            .post(`${END_POINT}/register`)
            .send(newUser);
        
        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    });

    test('Create account without email: Should fail', async () => {
        const newUser = {
            name: 'admin',
            password: '123456'
        };

        const response = await request(serve)
            .post(`${END_POINT}/register`)
            .send(newUser);
        
        const { error } = response.body;
        expect(error).toBe('O campo e-mail é obrigatório');
    });

    test('Create account without password: Should fail', async () => {
        const newUser = {
            name: 'admin',
            email: 'andrescar14@gmail.com'
        };

        const response = await request(serve)
            .post(`${END_POINT}/register`)
            .send(newUser);
        
        const { error } = response.body;
        expect(error).toBe('O campo senha é obrigatório');
    });

    test('Send email forgot password: Should pass', async () => {
        const newUser = {
            name: 'admin',
            email: 'andrescar14@gmail.com',
            password: '123456'
        };

        await ApiAuth.Create(newUser);

        const response = await request(serve)
            .post(`${END_POINT}/forgot-password`)
            .send({email: newUser.email});

        const { status } = response;
        expect(status).toBe(200);
    });

    test('Send email forgot password for fake user: Should fail', async () => {

        const response = await request(serve)
            .post(`${END_POINT}/forgot-password`)
            .send({email: 'fake@gmail.com'});

        const { error } = response.body;
        expect(error).toBe('Usuário não encontrado');

    });

    test('Reset password: Should pass', async () => {
        const newUser = {
            name: 'admin',
            email: 'andrescar14@gmail.com',
            password: '123456'
        };

        await ApiAuth.Create(newUser);

        await request(serve)
            .post(`${END_POINT}/forgot-password`)
            .send({email: newUser.email});

        const userDB = await User.findOne({ email: newUser.email }).select('+passwordResetToken');

        const newPassword = {
            email: newUser.email,
            token: userDB?.passwordResetToken,
            password: '654321'
        };

        const response = await request(serve)
            .post(`${END_POINT}/reset-password`)
            .send(newPassword);

        const { status } = response;
        expect(status).toBe(200);
    });

    test('Reset password fake user: Should fail', async () => {
        const fakeUser = {
            email: 'fake@gmail.com',
            token: '9ae9a051134d57f067642f8ec438f3ad',
            password: '654321'
        };

        const response = await request(serve)
            .post(`${END_POINT}/reset-password`)
            .send(fakeUser);

        const { error } = response.body;
        expect(error).toBe('Usuário não encontrado');
    });

    test('Reset password with invalid token: Should fail', async () => {

        const newUser = {
            name: 'admin',
            email: 'andrescar14@gmail.com',
            password: '123456'
        };

        await ApiAuth.Create(newUser);

        const newPassword = {
            email: newUser.email,
            token: '9ae9a051134d57f067642f8ec438f3ad',
            password: '654321'
        };

        const response = await request(serve)
            .post(`${END_POINT}/reset-password`)
            .send(newPassword);

        const { error } = response.body;
        expect(error).toBe('Token inválido');
    });
});