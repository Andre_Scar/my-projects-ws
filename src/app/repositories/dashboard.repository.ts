
import Activity from "../schemes/activity.schema";
import Story from "../schemes/story.schema";


export const TotalActivities = async (projectId: string, userId: string)  => {

    const countMy = await Activity
        .find({project: projectId, responsible: userId}).countDocuments();

    const countOthers = await Activity
        .find({project: projectId, responsible: {'$ne': userId}}).countDocuments();

    return {
        countMy,
        countOthers
    };
}

export const TotalStoriesByPriority = async (projectId: string, userId: string)  => {

    const stories = await Story.find({ project: projectId });

    let storiesByPriority: any = {};

    stories.forEach(e => {
        if(!storiesByPriority[e.priority]) {
            storiesByPriority[e.priority] = 0;
        }

        storiesByPriority[e.priority]++;
    });

    return storiesByPriority;
}

export const TotalActivitiesBySprint = async (projectId: string, userId: string) => {

    const stories = await Story.find({ project: projectId }).populate(['sprint']);

    let sprintsByStory:any = {};
    stories.forEach(e => {
        sprintsByStory[e._id.toString()] = e.sprint;
    });

    
    const activities = await Activity.find({ project: projectId });
    
    let activitiesBySprint:any = {};

    activities.forEach(e => {
        const sprint = sprintsByStory[e.story.toString()];
        const sprintId = sprint._id.toString();

        if(!activitiesBySprint[sprintId]) {
            activitiesBySprint[sprintId] = {
                sprintId: sprint._id,
                sprintName: sprint.name,
                toDo: 0,
                inProgress: 0,
                toVerify: 0,
                done: 0
            }
        }

        const status = e.status.toString();
        activitiesBySprint[sprintId][status]++; 
    });

    return Object.values(activitiesBySprint);
}

export const MyActivitiesByStatusAndSprint = async (projectId: string, userId: string) => {

    const stories = await Story.find({ project: projectId }).populate(['sprint']);

    let sprintsByStory:any = {};
    stories.forEach(e => {
        sprintsByStory[e._id] = e.sprint;
    });

    
    const activities = await Activity.find({ project: projectId, responsible: userId });
    
    let activitiesBySprint:any = {};
    
    activities.forEach(e => {
        const sprint = sprintsByStory[e.story];
        const sprintId = sprint._id.toString();

        if(!activitiesBySprint[sprintId]) {
            activitiesBySprint[sprintId] = {
                sprintId: sprint._id,
                sprintName: sprint.name,
                toDo: 0,
                inProgress: 0,
                toVerify: 0,
                done: 0
            }
        }

        activitiesBySprint[sprintId][e.status]++; 
    });

    return Object.values(activitiesBySprint);
}

export const MyActivitiesByPriorityAndSprint = async (projectId: string, userId: string) => {

    const stories = await Story.find({ project: projectId }).populate(['sprint']);

    let sprintsByStory:any = {};
    stories.forEach(e => {
        sprintsByStory[e._id] = e.sprint;
    });

    
    const activities = await Activity.find({ project: projectId, responsible: userId });
    
    let activitiesBySprint:any = {};
    
    activities.forEach(e => {
        const sprint = sprintsByStory[e.story];
        const sprintId = sprint._id.toString();

        if(!activitiesBySprint[sprintId]) {
            activitiesBySprint[sprintId] = {
                sprintId: sprintId,
                sprintName: sprint.name,
                urgent: 0,
                high: 0,
                medium: 0,
                low: 0
            }
        }

        activitiesBySprint[sprint._id][e.priority]++; 
    });

    return Object.values(activitiesBySprint);
}

export const TotalActivitiesByPriorityAndSprint = async (projectId: string, userId: string) => {

    const stories = await Story.find({ project: projectId }).populate(['sprint']);

    let sprintsByStory:any = {};
    stories.forEach(e => {
        sprintsByStory[e._id] = e.sprint;
    });

    
    const activities = await Activity.find({ project: projectId });
    
    let activitiesBySprint:any = {};
    
    activities.forEach(e => {
        const sprint = sprintsByStory[e.story];
        const sprintId = sprint._id.toString();

        if(!activitiesBySprint[sprintId]) {
            activitiesBySprint[sprintId] = {
                sprintId: sprintId,
                sprintName: sprint.name,
                urgent: 0,
                high: 0,
                medium: 0,
                low: 0
            }
        }

        activitiesBySprint[sprint._id][e.priority]++; 
    });

    return Object.values(activitiesBySprint);
}