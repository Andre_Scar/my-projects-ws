import { ObjectId } from 'mongodb';
import mongoose from '../database';
import { IActivityCustomFieldEntity } from '../entities/activityCustomField.entity';


export const ActivityCustomFieldSchema = new mongoose.Schema<IActivityCustomFieldEntity>({
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    activityType: {
        type: ObjectId,
        ref: 'ActivityType',
        required: true
    },
    project: {
        type: ObjectId,
        ref: 'Project',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    createdBy: {
        type: ObjectId,
        ref: 'User',
        required: true
    }
});


const ActivityCustomField = mongoose.model<IActivityCustomFieldEntity>('ActivityCustomField', ActivityCustomFieldSchema);

export default ActivityCustomField;