import mongoose from "../database";
import { IActivityCustomFieldEntity } from "./activityCustomField.entity";
import { IActivityTypeEntity } from "./activityType.entity";
import { IProjectEntity } from "./project.entity";
import { IStoryEntity, Priority } from "./story.entity";
import { IUserEntity } from "./user.entity";


export interface IActivityEntity extends mongoose.Document { 
    count: number;
    title: string;
    description?: string;
    responsible: IUserEntity["_id"];
    type: IActivityTypeEntity["_id"];
    status: ActivityStatus;
    priority: Priority;
    story: IStoryEntity["_id"];
    project: IProjectEntity["_id"];
    customFields: ICustomField[];
    createdAt?: Date;
    createdBy: IUserEntity["_id"];
};

export interface ICustomField {
    activityCustomField: IActivityCustomFieldEntity["_id"];
    valueText?: string | null;
    valueNumber?: number | null;
    valueDate?: Date | null;
}

export enum ActivityStatus {
    TO_DO = 'toDo',
    IN_PROGRESS = 'inProgress',
    TO_VERIFY = 'toVerify',
    DONE = 'done'
}