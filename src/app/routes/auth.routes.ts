import express from 'express';

import * as authController from '../controllers/auth.controller';


const router = express.Router();


router.post('/register', authController.Create);
router.post('/authenticate', authController.Authenticate);
router.post('/forgot-password', authController.ForgotPassword);
router.post('/reset-password', authController.ResetPassword);
router.post('/google', authController.Google);


export default (app: any) => app.use('/auth/v1', router);