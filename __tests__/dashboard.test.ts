import request from "supertest";
import { ApiActivity } from "../.jest/api/activity.api";
import { ApiAuth } from "../.jest/api/auth.api";
import { ApiProject } from "../.jest/api/project.api";
import { ApiSprint } from "../.jest/api/sprint.api";
import { ApiStory } from "../.jest/api/story.api";
import { users } from "../.jest/mocks/user.mock";
import { commonAuthHeaders, dropDatabase } from "../.jest/utils";
import { ActivityStatus } from "../src/app/entities/activity.entity";
import { Priority } from "../src/app/entities/story.entity";
import { serve } from "../src/server";

const END_POINT: string = `/dashboard/v1`;

describe('Dashboard', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {

        await dropDatabase();
        await serve.close();

    });

    beforeEach(async () => {

        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();

    });
    //#endregion

    test('Total activities: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin);
        await ApiActivity.Create('activity 2', adminUserId, typeId, projectId, storyId, tokenAdmin);
        await ApiActivity.Create('activity 3', defaultUserId, typeId, projectId, storyId, tokenAdmin);

        const response = await request(serve)
            .get(`${END_POINT}/${projectId}/total-activities`)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { countMy, countOthers } = response.body;
        
        expect(countMy).toBe(2);
        expect(countOthers).toBe(1);
    
    });

    test('Total stories by priority: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin, Priority.LOW);
        await ApiStory.Create('Story 2', sprintId, projectId, tokenAdmin);
        await ApiStory.Create('Story 3', sprintId, projectId, tokenAdmin);

        const response = await request(serve)
            .get(`${END_POINT}/${projectId}/total-stories-by-priority`)
            .set(commonAuthHeaders(tokenAdmin));

        const { medium, low } = response.body
        
        expect(medium).toBe(2);
        expect(low).toBe(1);
    
    });


    test('Total activities by sprint: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin);
        await ApiActivity.Create('activity 2', adminUserId, typeId, projectId, storyId, tokenAdmin, ActivityStatus.IN_PROGRESS);
        await ApiActivity.Create('activity 3', defaultUserId, typeId, projectId, storyId, tokenAdmin);

        const response = await request(serve)
            .get(`${END_POINT}/${projectId}/total-activities-by-sprint`)
            .set(commonAuthHeaders(tokenAdmin));
        
        expect(response.body[0].sprintName).toBe('Sprint 1');
        expect(response.body[0].toDo).toBe(2);
        expect(response.body[0].inProgress).toBe(1);
    
    });

    test('My activities by status and sprint: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin);
        await ApiActivity.Create('activity 2', adminUserId, typeId, projectId, storyId, tokenAdmin, ActivityStatus.IN_PROGRESS);
        await ApiActivity.Create('activity 3', defaultUserId, typeId, projectId, storyId, tokenAdmin);

        const response = await request(serve)
            .get(`${END_POINT}/${projectId}/my-activities-by-status-and-sprint`)
            .set(commonAuthHeaders(tokenAdmin));

        expect(response.body[0].sprintName).toBe('Sprint 1');
        expect(response.body[0].toDo).toBe(1);
        expect(response.body[0].inProgress).toBe(1);
    
    });


    test('My activities by priority and sprint: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin, ActivityStatus.TO_DO, Priority.LOW);
        await ApiActivity.Create('activity 2', adminUserId, typeId, projectId, storyId, tokenAdmin, ActivityStatus.IN_PROGRESS);
        await ApiActivity.Create('activity 3', defaultUserId, typeId, projectId, storyId, tokenAdmin);

        const response = await request(serve)
            .get(`${END_POINT}/${projectId}/my-activities-by-priority-and-sprint`)
            .set(commonAuthHeaders(tokenAdmin));

        expect(response.body[0].sprintName).toBe('Sprint 1');
        expect(response.body[0].high).toBe(1);
        expect(response.body[0].low).toBe(1);
    
    });

    test('Total activities by priority and sprint: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin, ActivityStatus.TO_DO, Priority.LOW);
        await ApiActivity.Create('activity 2', adminUserId, typeId, projectId, storyId, tokenAdmin, ActivityStatus.IN_PROGRESS);
        await ApiActivity.Create('activity 3', defaultUserId, typeId, projectId, storyId, tokenAdmin);

        const response = await request(serve)
            .get(`${END_POINT}/${projectId}/total-activities-by-priority-and-sprint`)
            .set(commonAuthHeaders(tokenAdmin));

        expect(response.body[0].sprintName).toBe('Sprint 1');
        expect(response.body[0].high).toBe(2);
        expect(response.body[0].low).toBe(1);
    
    });


});