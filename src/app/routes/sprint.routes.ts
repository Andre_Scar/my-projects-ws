import express from 'express';
import authMiddleware from './../middlewares/auth';

import * as sprintController from '../controllers/sprint.controller';


const router = express.Router();

router.use(authMiddleware);

router.post('/', sprintController.Save);
router.put('/:id', sprintController.Update);
router.get('/:id', sprintController.FindById);
router.post('/list', sprintController.List);
router.delete('/:id', sprintController.Delete);
router.get('/export-csv/:id', sprintController.ExportCSV);


export default (app: any) => app.use('/sprint/v1', router);