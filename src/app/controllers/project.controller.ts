import * as projectRepository  from '../repositories/project.repository';
import { IRequest, IResponse } from '../types/express';


export const Save = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await projectRepository.Create(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Update = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await projectRepository.Update(request.body, request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const FindById = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await projectRepository.FindById(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const List = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await projectRepository.List(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Delete = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await projectRepository.Delete(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const ChangeIsFavorite = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await projectRepository.ChangeIsFavorite(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}