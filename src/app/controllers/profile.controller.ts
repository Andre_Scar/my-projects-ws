import * as profileRepository  from '../repositories/profile.repository';
import { IRequest, IResponse } from '../types/express';



export const Update = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await profileRepository.Update(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const FindById = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await profileRepository.FindById(request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}
