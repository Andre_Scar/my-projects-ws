import mongoose from "../database";
import { IUserEntity } from "./user.entity";



export interface IProjectEntity extends mongoose.Document { 
    name: string;
    description?: string;
    countActivities: number;
    countDone: number;
    countDoing: number;
    countVerify: number;
    users: IUserProjectEntity[];
    createdAt?: Date;
    createdBy: IUserEntity["_id"];
};

export interface IUserProjectEntity {
    user: IUserEntity["_id"];
    permission: string;
    isFavorite: boolean;
}