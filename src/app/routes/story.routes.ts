import express from 'express';
import authMiddleware from './../middlewares/auth';

import * as storyController from '../controllers/story.controller';


const router = express.Router();

router.use(authMiddleware);

router.post('/', storyController.Save);
router.put('/:id', storyController.Update);
router.get('/:id', storyController.FindById);
router.post('/list-with-activities', storyController.ListWithActivities);
router.delete('/:id', storyController.Delete);


export default (app: any) => app.use('/story/v1', router);