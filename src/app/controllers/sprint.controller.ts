import * as sprintRepository  from '../repositories/sprint.repository';
import { IRequest, IResponse } from '../types/express';


export const Save = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await sprintRepository.Create(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Update = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await sprintRepository.Update(request.body, request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const FindById = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await sprintRepository.FindById(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const List = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await sprintRepository.List(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Delete = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await sprintRepository.Delete(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const ExportCSV = async (request: IRequest, response: IResponse) => {
    try {

        response.attachment('filename.csv');
        return response.send(await sprintRepository.ExportCSV(request.params.id));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}