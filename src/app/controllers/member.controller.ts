import * as memberRepository  from '../repositories/member.repository';
import { IRequest, IResponse } from '../types/express';


export const Update = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await memberRepository.Update(request.body, request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const List = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await memberRepository.List(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Delete = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await memberRepository.Delete(request.params.id, request.params.projectId, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const GenerateLink = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await memberRepository.GenerateLink(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const Ingress = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await memberRepository.Ingress(request.params.code, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}
