import { IActivityCustomFieldModel, IFilterByActivityId, IListActivityCustomFieldModel } from "../models/activityCustomField.models";
import { Permission } from "../models/project.models";
import ActivityCustomField from "../schemes/activityCustomField.schema";
import ActivityType from "../schemes/activityType.schema";
import Project from "../schemes/project.schema";
import { ExceptionService } from "../services/exception.service";
import { IPaginationQuery } from "../types/pagination";



export const Create = async (activityCustomField: IActivityCustomFieldModel, userId: string)  => {

    const { 
        name,
        type,
        activityTypeId,
        projectId
    } = activityCustomField;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    if(!type) {
        throw ExceptionService.GenerateException(400, 'O campo tipo é obrigatório');
    }

    if(!activityTypeId) {
        throw ExceptionService.GenerateException(400, 'O campo tipo de atividade é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para criar campo personalizado');
    }

    const activityCustomFieldBD = await ActivityCustomField.create({ 
        name,
        type,
        activityType: activityTypeId,
        project: projectId,
        createdBy: userId
    });

    return { activityCustomField: activityCustomFieldBD };
}


export const Update = async (activityCustomField: IActivityCustomFieldModel, activityCustomFieldId: string, userId: string)  => {

    const { 
        name,
        projectId
    } = activityCustomField;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para editar o campo personalizado');
    }

    const activityCustomFieldBD = await ActivityCustomField.findByIdAndUpdate(activityCustomFieldId, { 
        name
    }, { new: true });

    return { activityCustomField: activityCustomFieldBD };
}


export const FindById = async (id: string, userId: string) => {

    const activityCustomFieldBD = await ActivityCustomField.findById(id).limit(1);

    if(!activityCustomFieldBD) {
        throw ExceptionService.GenerateException(400, 'Campo personalizado não encontrado');
    }

    return { 
        _id: activityCustomFieldBD._id,
        name: activityCustomFieldBD.name,
        type: activityCustomFieldBD.type,
        activityTypeId: activityCustomFieldBD.activityType
    };
}


export const List = async (query: IPaginationQuery, userId: string) => {

    if(query.filter === null || Object.keys(query.filter).length == 0) {
        return {
            page: query.page,
            pageSize: query.pageSize,
            results: [],
            count: 0
        };
    }

    const activityCustomFields = await ActivityCustomField
        .find(query.filter)
        .skip(query.page * query.pageSize)
        .limit(query.pageSize)
        .populate(['createdBy', 'activityType']);

    const count = await ActivityCustomField.find(query.filter).countDocuments();

    const results:IListActivityCustomFieldModel[] = activityCustomFields.map(activityCustomField => {
        return {
            _id: activityCustomField._id,
            name: activityCustomField.name,
            type: activityCustomField.type,
            activityTypeName: activityCustomField.activityType.name,
            createdByName: activityCustomField.createdBy.name,
            projectId: activityCustomField.project
        }
    });

    return {
        page: query.page,
        pageSize: query.pageSize,
        results: results,
        count: count
    };
}

export const ListAll = async (filter: IFilterByActivityId, userId: string) => {

    const { activityTypeId } = filter;

    const activityCustomFields = await ActivityCustomField
        .find({ activityType: activityTypeId })
        .populate(['createdBy']);

    const results:IListActivityCustomFieldModel[] = activityCustomFields.map(activityCustomField => {
        return {
            _id: activityCustomField._id,
            name: activityCustomField.name,
            type: activityCustomField.type,
            activityTypeName: activityCustomField.activityType.name,
            createdByName: activityCustomField.createdBy.name,
            projectId: activityCustomField.project
        }
    });

    return results;
}

export const Delete = async (id: string, userId: string) => {
    
    const activityCustomFieldBD = await ActivityCustomField.findOne({_id: id});

    if(!activityCustomFieldBD) {
        throw ExceptionService.GenerateException(400, 'Campo personalizado não encontrado');
    }

    const projectBD = await Project.findOne({_id: activityCustomFieldBD.project});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const user = projectBD.users.find((item) => item.user.toString() === userId);

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para deletar campo personalizado');
    }

    activityCustomFieldBD.deleteOne();
}