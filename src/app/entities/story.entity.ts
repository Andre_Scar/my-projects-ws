import mongoose from "../database";
import { IProjectEntity } from "./project.entity";
import { ISprintEntity } from "./sprint.entity";
import { IUserEntity } from "./user.entity";


export interface IStoryEntity extends mongoose.Document { 
    count: number;
    description: string;
    priority: Priority;
    sprint: ISprintEntity["_id"];
    project: IProjectEntity["_id"];
    createdAt?: Date;
    createdBy: IUserEntity["_id"];
};

export enum Priority {
    URGENT = 'urgent',
    HIGH = 'high',
    MEDIUM = 'medium',
    LOW = 'low'
}