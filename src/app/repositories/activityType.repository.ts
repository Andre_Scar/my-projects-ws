
import { IActivityTypeModel, IFilterByProjectId, IListActivityTypeModel } from "../models/activityType.models";
import { Permission } from "../models/project.models";
import ActivityType from "../schemes/activityType.schema";
import Project from "../schemes/project.schema";
import { ExceptionService } from "../services/exception.service";
import { IPaginationQuery } from "../types/pagination";



export const Create = async (activityType: IActivityTypeModel, userId: string)  => {

    const { 
        name,
        description,
        projectId
    } = activityType;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para criar tipo de atividade');
    }

    const activityTypeBD = await ActivityType.create({ 
        name,
        description,
        project: projectId,
        createdBy: userId
    });

    return { activityType: activityTypeBD };
}


export const Update = async (activityType: IActivityTypeModel, activityTypeId: string, userId: string)  => {

    const { 
        name,
        description,
        projectId
    } = activityType;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para editar o tipo de atividade');
    }

    const activityTypeBD = await ActivityType.findByIdAndUpdate(activityTypeId, { 
        name,
        description
    }, { new: true });

    return { activityType: activityTypeBD };
}


export const FindById = async (id: string, userId: string) => {

    const activityTypeBD = await ActivityType.findById(id).limit(1);

    if(!activityTypeBD) {
        throw ExceptionService.GenerateException(400, 'Tipo de atividade não encontrada');
    }

    return { 
        _id: activityTypeBD._id,
        name: activityTypeBD.name,
        description: activityTypeBD.description
    };
}


export const List = async (query: IPaginationQuery, userId: string) => {

    if(query.filter === null || Object.keys(query.filter).length == 0) {
        return {
            page: query.page,
            pageSize: query.pageSize,
            results: [],
            count: 0
        };
    }

    const activityTypes = await ActivityType
        .find(query.filter)
        .skip(query.page * query.pageSize)
        .limit(query.pageSize)
        .populate(['createdBy']);

    const count = await ActivityType.find(query.filter).countDocuments();

    const results:IListActivityTypeModel[] = activityTypes.map(activityType => {
        return {
            _id: activityType._id,
            name: activityType.name,
            description: activityType.description,
            createdByName: activityType.createdBy.name,
            projectId: activityType.project
        }
    });

    return {
        page: query.page,
        pageSize: query.pageSize,
        results: results,
        count: count
    };
}

export const ListAll = async (filter: IFilterByProjectId, userId: string) => {

    const { projectId } = filter;

    const activityTypes = await ActivityType
        .find({
            '$or': [
                {project: projectId},
                { project: null },
            ]
        })
        .populate(['createdBy']);

    const results:IListActivityTypeModel[] = activityTypes.map(activityType => {
        return {
            _id: activityType._id,
            name: activityType.name,
            description: activityType.description,
            createdByName: activityType.createdBy.name,
            projectId: activityType.project
        }
    });

    return results;
}

export const Delete = async (id: string, userId: string) => {
    
    const activityTypeBD = await ActivityType.findOne({_id: id});

    if(!activityTypeBD) {
        throw ExceptionService.GenerateException(400, 'Tipo de atividade não encontrado');
    }

    const projectBD = await Project.findOne({_id: activityTypeBD.project});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const user = projectBD.users.find((item) => item.user.toString() === userId);

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para deletar o tipo de atividade');
    }

    activityTypeBD.deleteOne();
}

export const NewPorject = async (projectId: string, userId: string) => {

    await ActivityType.create({ 
        name: 'Bug',
        description: 'Erro ou falha que ocorre em um sistema.',
        project: projectId,
        createdBy: userId
    });

    await ActivityType.create({ 
        name: 'Feature',
        description: 'Funcionalidade do sistema.',
        project: projectId,
        createdBy: userId
    });

}