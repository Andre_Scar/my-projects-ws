import express from 'express';
import authMiddleware from './../middlewares/auth';

import * as dashboardController from '../controllers/dashboard.controller';


const router = express.Router();

router.use(authMiddleware);

router.get('/:id/total-activities', dashboardController.TotalActivities);
router.get('/:id/total-stories-by-priority', dashboardController.TotalStoriesByPriority);
router.get('/:id/total-activities-by-sprint', dashboardController.TotalActivitiesBySprint);
router.get('/:id/my-activities-by-status-and-sprint', dashboardController.MyActivitiesByStatusAndSprint);
router.get('/:id/my-activities-by-priority-and-sprint', dashboardController.MyActivitiesByPriorityAndSprint);
router.get('/:id/total-activities-by-priority-and-sprint', dashboardController.TotalActivitiesByPriorityAndSprint);


export default (app: any) => app.use('/dashboard/v1', router);

