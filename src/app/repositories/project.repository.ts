import { IChangeIsFavoriteModel, IListProjectModel, IProjectModel, Permission } from "../models/project.models";
import Project from "../schemes/project.schema";
import { ExceptionService } from "../services/exception.service";
import { IPaginationQuery } from "../types/pagination";
import * as activityTypeRepository  from '../repositories/activityType.repository';

export const Create = async (projectModel: IProjectModel, userId: string)  => {

    const { 
        name,
        description,
        isFavorite
    } = projectModel;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    const projectBD = await Project.create({ 
        name,
        description,
        countActivities: 0,
        countDone: 0,
        countDoing: 0,
        countVerify: 0,
        users: [
            {
                user: userId,
                permission: Permission.MANAGER,
                isFavorite: isFavorite || false
            }
        ],
        createdBy: userId
    });


    await activityTypeRepository.NewPorject(projectBD._id, userId);

    return { project: projectBD };
}

export const Update = async (projectModel: IProjectModel, projectId: string, userId: string)  => {

    const { 
        name,
        description,
        isFavorite
    } = projectModel;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para editar o projeto');
    }

    projectBD.name = name;
    projectBD.description = description;
    projectBD.users[userIndex].isFavorite = isFavorite;

    return await projectBD.save();
}

export const FindById = async (id: string, userId: string) => {

    const projectBD = await Project.findById(id).limit(1);

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const isFavorite = projectBD.users.find((item) => item.user.toString() === userId)?.isFavorite === true;

    return { 
        _id: projectBD._id,
        name: projectBD.name,
        description: projectBD.description,
        isFavorite: isFavorite
    };
}

export const List = async (query: IPaginationQuery, userId: string) => {

    const projects = await Project
        .find(query.filter)
        .skip(query.page * query.pageSize)
        .limit(query.pageSize)
        .populate(['createdBy']);

    const count = await Project.find(query.filter).countDocuments();

    const results:IListProjectModel[] = projects.map(project => {
        const user = project.users.find((item) => item.user.toString() === userId);

        return {
            _id: project._id,
            name: project.name,
            description: project.description,
            createdByName: project.createdBy.name,
            countActivities: project.countActivities,
            countDone: project.countDone,
            countDoing: project.countDoing,
            countVerify: project.countVerify,
            isFavorite:  user ? user.isFavorite : false,
            permission: user ? user.permission : Permission.DEFAULT,
        }
    });

    return {
        page: query.page,
        pageSize: query.pageSize,
        results: results,
        count: count
    };
}

export const Delete = async (id: string, userId: string) => {
    
    const projectBD = await Project.findOne({_id: id});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const user = projectBD.users.find((item) => item.user.toString() === userId);

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para deletar o projeto');
    }

    projectBD.deleteOne();
}

export const ChangeIsFavorite = async (changeIsFavoriteModel: IChangeIsFavoriteModel, userId: string) => {
    
    const projectBD = await Project.findOne({_id: changeIsFavoriteModel.projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    projectBD.users.forEach(item => {
        if(item.user.toString() === userId) {
            item.isFavorite = changeIsFavoriteModel.isFavorite;
        }
    });

    await projectBD.save();

    return { 
        _id: projectBD._id,
        name: projectBD.name,
        description: projectBD.description,
        isFavorite: changeIsFavoriteModel.isFavorite
    }; 
}