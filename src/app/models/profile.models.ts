
export interface IProfileModel { 
    name: string;
    email: string;
    countMyProjects?: number;
    countSharedWithMe?: number;
};