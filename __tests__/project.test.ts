import request from "supertest";
import { ApiAuth } from "../.jest/api/auth.api";
import { commonAuthHeaders, dropDatabase } from "../.jest/utils";
import { serve } from "../src/server";
import { users } from "../.jest/mocks/user.mock";
import Project from "../src/app/schemes/project.schema";
import { ApiProject } from "../.jest/api/project.api";

const END_POINT: string = `/project/v1`;

describe('Projects', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {
        await dropDatabase();
        await serve.close();
    });

    beforeEach(async () => {
        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();

    });
    //#endregion

    test('Create project: Should pass', async () => {
        const project = {
            name: 'Projeto 1',
            description: 'teste',
            isFavorite: true
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(project)
            .set(commonAuthHeaders(tokenAdmin));
        
        const {_id, name, description, users} = response.body.project;
        
        expect(_id).not.toBe(null);
        expect(name).toBe(project.name);
        expect(description).toBe(project.description);
        expect(users[0].isFavorite).toBe(project.isFavorite);
    });

    test('Create unnamed project: Should fail', async () => {
        const project = {
            name: null,
            isFavorite: true
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(project)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    });

    test('Update project: Should pass', async () => {
        const project = {
            name: 'Projeto 1',
            description: 'teste',
            isFavorite: true
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(project)
            .set(commonAuthHeaders(tokenAdmin));

        const projectId = response.body.project._id.toString();
        
        const updateProject = {
            name: 'Projeto 2',
            description: 'teste 2',
            isFavorite: false
        }
        await request(serve)
            .put(`${END_POINT}/${projectId}`)
            .send(updateProject)
            .set(commonAuthHeaders(tokenAdmin));

        const projectBD = await Project.findOne({_id: projectId})
        
        expect(projectBD).not.toBe(null);
        expect(projectBD?._id.toString()).toBe(projectId);
        expect(projectBD?.name).toBe(updateProject.name);
        expect(projectBD?.description).toBe(updateProject.description);
        expect(projectBD?.users[0].isFavorite).toBe(updateProject.isFavorite);
    });

    test('Update project name to null : Should fail', async () => {
        const project = {
            name: 'Projeto 1',
            description: 'teste',
            isFavorite: true
        };

        const responseNew = await request(serve)
            .post(`${END_POINT}/`)
            .send(project)
            .set(commonAuthHeaders(tokenAdmin));

        const projectId = responseNew.body.project._id.toString();
        
        const updateProject = {
            name: null,
            description: 'teste 2',
            isFavorite: false
        }
        const response = await request(serve)
            .put(`${END_POINT}/${projectId}`)
            .send(updateProject)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    });

    test('Update fake project: Should fail', async () => {

        const fakeProjectId = '5fa0af509db40713507b0767';

        const updateProject = {
            name: 'Projeto 2',
            description: 'teste 2',
            isFavorite: false
        }

        const response = await request(serve)
            .put(`${END_POINT}/${fakeProjectId}`)
            .send(updateProject)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    });

    test('Update project by default user: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        
        const response = await request(serve)
            .put(`${END_POINT}/${projectDB._id}`)
            .send({
                name: 'Projeto 2',
                description: 'teste 2',
                isFavorite: true
            })
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para editar o projeto');

    });

    test('Find project by id: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        
        const response = await request(serve)
            .get(`${END_POINT}/${projectDB._id}`)
            .set(commonAuthHeaders(tokenDefault));

        const { _id, name, description, isFavorite } = response.body;

        expect(_id).not.toBe(null);
        expect(name).toBe('Projeto 1');
        expect(description).toBe(undefined);
        expect(isFavorite).toBe(false);
    });

    test('Find fake project by id: Should fail', async () => {

        const fakeProjectId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .get(`${END_POINT}/${fakeProjectId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    });

    test('List projects: Should pass', async () => {

        for(var i = 0; i < 5; i++) {
            await ApiProject.Create(`Projeto ${i}`, adminUserId, defaultUserId);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({
                pageSize: 10,
                page: 0,
                filter: null
            })
            .set(commonAuthHeaders(tokenAdmin));

        const { count, results } = response.body;

        expect(count).toBe(5);
        expect(results.length).toBe(5);
        expect(results[0].name).toBe('Projeto 0');
        expect(results[1].name).toBe('Projeto 1');
        expect(results[2].name).toBe('Projeto 2');
        expect(results[3].name).toBe('Projeto 3');
        expect(results[4].name).toBe('Projeto 4');
    });

    test('List projects filtered by name: Should pass', async () => {

        for(var i = 0; i < 5; i++) {
            await ApiProject.Create(`Projeto ${i}`, adminUserId, defaultUserId);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({
                pageSize: 10,
                page: 0,
                filter: {name: {'$regex' : '1', '$options' : 'i'}}
            })
            .set(commonAuthHeaders(tokenAdmin));

        const { count, results } = response.body;

        expect(count).toBe(1);
        expect(results.length).toBe(1);
        expect(results[0].name).toBe('Projeto 1');

    });

    test('Dalete project by user admin: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        
        const response = await request(serve)
            .delete(`${END_POINT}/${projectDB._id}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { status } = response;
        expect(status).toBe(200);

    });

    test('Dalete fake project: Should fail', async () => {

        const fakeProjectId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${fakeProjectId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');

    });

    test('Dalete project by user default: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        
        const response = await request(serve)
            .delete(`${END_POINT}/${projectDB._id}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para deletar o projeto');
        
    });

    test('Change isFavorite of project: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        
        await request(serve)
            .post(`${END_POINT}/change-is-favorite`)
            .send({
                projectId: projectDB._id,
                isFavorite: true
            })
            .set(commonAuthHeaders(tokenDefault));

        const response = await request(serve)
            .get(`${END_POINT}/${projectDB._id}`)
            .set(commonAuthHeaders(tokenDefault));

        const { isFavorite } = response.body;
        expect(isFavorite).toBe(true);
    });

    test('Change isFavorite of fake project: Should fail', async () => {

        const fakeProjectId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .post(`${END_POINT}/change-is-favorite`)
            .send({
                projectId: fakeProjectId,
                isFavorite: true
            })
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');

    });
});