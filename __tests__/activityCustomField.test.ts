import request from "supertest";
import { ApiActivity } from "../.jest/api/activity.api";
import { ApiAuth } from "../.jest/api/auth.api";
import { ApiProject } from "../.jest/api/project.api";
import { users } from "../.jest/mocks/user.mock";
import { commonAuthHeaders, dropDatabase } from "../.jest/utils";
import { CustomFieldType } from "../src/app/entities/activityCustomField.entity";
import { serve } from "../src/server";

const END_POINT: string = `/activity-custom-field/v1`;

describe('Custom field', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {

        await dropDatabase();
        await serve.close();

    });

    beforeEach(async () => {

        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();

    });
    //#endregion

    test('Create activity custom field: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomField = {
            name: 'Texto',
            type: CustomFieldType.TEXT,
            activityTypeId,
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityCustomField)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { name, type, activityType, project, createdBy } = response.body.activityCustomField;
        
        expect(name).toBe(activityCustomField.name);
        expect(type).toBe(CustomFieldType.TEXT);
        expect(activityType).toBe(activityTypeId);
        expect(project).toBe(projectId);
        expect(createdBy).toBe(adminUserId);
    
    });

    test('Create activity custom field no name: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomField = {
            name: null,
            type: CustomFieldType.TEXT,
            activityTypeId,
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityCustomField)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    
    });

    test('Create activity custom field no type: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomField = {
            name: 'Texto',
            type: null,
            activityTypeId,
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityCustomField)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo tipo é obrigatório');
    
    });

    test('Create activity custom field no activity type: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityCustomField = {
            name: 'Texto',
            type: CustomFieldType.TEXT,
            activityTypeId: null,
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityCustomField)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo tipo de atividade é obrigatório');
    
    });

    test('Create activity custom field in fake project: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const fakeProjectId = '5fa0af509db40713507b0767';

        const activityCustomField = {
            name: 'Texto',
            type: CustomFieldType.TEXT,
            activityTypeId,
            projectId: fakeProjectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityCustomField)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    
    });

    test('Create activity custom field by default user: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomField = {
            name: 'Texto',
            type: CustomFieldType.TEXT,
            activityTypeId,
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityCustomField)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para criar campo personalizado');
    
    });

    test('Update activity custom field: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomFieldDB = await ApiActivity.CustomField('Texto', CustomFieldType.TEXT, activityTypeId, projectId, tokenAdmin);
        const activityCustomFieldId = activityCustomFieldDB ? activityCustomFieldDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${activityCustomFieldId}`)
            .send({
                name: 'Texto 2',
                projectId
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { name } = response.body.activityCustomField;
        
        expect(name).toBe('Texto 2');
    
    });

    test('Update activity custom field no name: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomFieldDB = await ApiActivity.CustomField('Texto', CustomFieldType.TEXT, activityTypeId, projectId, tokenAdmin);
        const activityCustomFieldId = activityCustomFieldDB ? activityCustomFieldDB._id.toString() : '';

        const response = await request(serve)
            .put(`${END_POINT}/${activityCustomFieldId}`)
            .send({
                name: null,
                projectId
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    
    });

    test('Update activity custom field in fake project: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const fakeProjectId = '5fa0af509db40713507b0767';

        const activityCustomFieldDB = await ApiActivity.CustomField('Texto', CustomFieldType.TEXT, activityTypeId, projectId, tokenAdmin);
        const activityCustomFieldId = activityCustomFieldDB ? activityCustomFieldDB._id.toString() : '';

        const response = await request(serve)
            .put(`${END_POINT}/${activityCustomFieldId}`)
            .send({
                name: 'Texto 2',
                projectId: fakeProjectId
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    
    });

    test('Update activity custom field by default user: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomFieldDB = await ApiActivity.CustomField('Texto', CustomFieldType.TEXT, activityTypeId, projectId, tokenAdmin);
        const activityCustomFieldId = activityCustomFieldDB ? activityCustomFieldDB._id.toString() : '';

        const response = await request(serve)
            .put(`${END_POINT}/${activityCustomFieldId}`)
            .send({
                name: 'Texto 2',
                projectId
            })
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para editar o campo personalizado');
    
    });

    test('Find activity custom field by id: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeDBId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomFieldDB = await ApiActivity.CustomField('Texto', CustomFieldType.TEXT, activityTypeDBId, projectId, tokenAdmin);
        const activityCustomFieldId = activityCustomFieldDB ? activityCustomFieldDB._id.toString() : '';
        
        const response = await request(serve)
            .get(`${END_POINT}/${activityCustomFieldId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { _id, name, type, activityTypeId } = response.body;
        
        expect(_id).toBe(activityCustomFieldId);
        expect(name).toBe('Texto');
        expect(type).toBe(CustomFieldType.TEXT);
        expect(activityTypeId).toBe(activityTypeDBId);

    });

    test('Find fake activity custom field by id: Should pass', async () => {

        const fakeActivityCustomFieldId = '5fa0af509db40713507b0767';

        const response = await request(serve)
            .get(`${END_POINT}/${fakeActivityCustomFieldId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Campo personalizado não encontrado');
    });

    test('List activity custom field: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeDBId = activityTypeDB ? activityTypeDB._id.toString() : '';

        for(var i = 0; i < 5; i++) {
            await ApiActivity.CustomField(`Texto ${i}`, CustomFieldType.TEXT, activityTypeDBId, projectId, tokenAdmin);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({
                pageSize: 10,
                page: 0,
                filter: {project: projectId}
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { count, results } = response.body;

        expect(count).toBe(5);
        expect(results.length).toBe(5);
        expect(results[0].name).toBe('Texto 0');
        expect(results[1].name).toBe('Texto 1');
        expect(results[2].name).toBe('Texto 2');
        expect(results[3].name).toBe('Texto 3');
        expect(results[4].name).toBe('Texto 4');
    });

    test('List activity custom field filtered by name: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeDBId = activityTypeDB ? activityTypeDB._id.toString() : '';

        for(var i = 0; i < 5; i++) {
            await ApiActivity.CustomField(`Texto ${i}`, CustomFieldType.TEXT, activityTypeDBId, projectId, tokenAdmin);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({
                pageSize: 10,
                page: 0,
                filter: {
                    project: projectId,
                    name: {'$regex' : '1', '$options' : 'i'}
                }
            })
            .set(commonAuthHeaders(tokenAdmin));

        const { count, results } = response.body;

        expect(count).toBe(1);
        expect(results.length).toBe(1);
        expect(results[0].name).toBe('Texto 1');

    });

    test('List all activity custom field: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        for(var i = 0; i < 5; i++) {
            await ApiActivity.CustomField(`Texto ${i}`, CustomFieldType.TEXT, activityTypeId, projectId, tokenAdmin);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list-all`)
            .send({ activityTypeId })
            .set(commonAuthHeaders(tokenAdmin));

        expect(response.body.length).toBe(5);
        expect(response.body[0].name).toBe('Texto 0');
        expect(response.body[1].name).toBe('Texto 1');
        expect(response.body[2].name).toBe('Texto 2');
        expect(response.body[3].name).toBe('Texto 3');
        expect(response.body[4].name).toBe('Texto 4');
    });

    test('Dalete activity custom field by user admin: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomFieldDB = await ApiActivity.CustomField('Texto', CustomFieldType.TEXT, activityTypeId, projectId, tokenAdmin);
        const activityCustomFieldId = activityCustomFieldDB ? activityCustomFieldDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${activityCustomFieldId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { status } = response;
        expect(status).toBe(200);

    });

    test('Dalete fake activity custom field: Should fail', async () => {

        const fakeSprintId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${fakeSprintId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Campo personalizado não encontrado');

    });

    test('Dalete activity custom field by user default: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomFieldDB = await ApiActivity.CustomField('Texto', CustomFieldType.TEXT, activityTypeId, projectId, tokenAdmin);
        const activityCustomFieldId = activityCustomFieldDB ? activityCustomFieldDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${activityCustomFieldId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para deletar campo personalizado');
        
    });

    test('Dalete activity custom field with project not found: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityCustomFieldDB = await ApiActivity.CustomField('Texto', CustomFieldType.TEXT, activityTypeId, projectId, tokenAdmin);
        const activityCustomFieldId = activityCustomFieldDB ? activityCustomFieldDB._id.toString() : '';
        
        await ApiProject.Delete(projectId);

        const response = await request(serve)
            .delete(`${END_POINT}/${activityCustomFieldId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
        
    });
});