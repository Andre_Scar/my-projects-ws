import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

mongoose.set('useFindAndModify', false);
mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

mongoose.connect(process.env.MONGO_URL || '');
mongoose.Promise = global.Promise;


export default mongoose;