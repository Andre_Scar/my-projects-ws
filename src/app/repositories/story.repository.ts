import { Permission } from "../models/project.models";
import { IStoryModel } from "../models/story.models";
import Activity from "../schemes/activity.schema";
import Project from "../schemes/project.schema";
import Story from "../schemes/story.schema";
import { ExceptionService } from "../services/exception.service";
import * as activityRepository  from './activity.repository';


export const Create = async (storyModel: IStoryModel, userId: string)  => {

    const { 
        description,
        priority,
        sprintId,
        projectId
    } = storyModel;

    if(!description || description === '') {
        throw ExceptionService.GenerateException(400, 'O campo história é obrigatório');
    }

    if(!priority) {
        throw ExceptionService.GenerateException(400, 'O campo prioridade é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para cadastrar história');
    }

    const lastStoryDB = await Story.findOne({project: projectId}).sort({count: -1});

    const storyBD = await Story.create({ 
        count: lastStoryDB ? lastStoryDB.count + 1 : 1,
        description,
        priority,
        sprint: sprintId,
        project: projectId,
        createdBy: userId
    });

    return { story: storyBD };
}


export const Update = async (storyModel: IStoryModel, storyId: string, userId: string)  => {

    const { 
        description,
        priority,
        projectId
    } = storyModel;

    if(!description || description === '') {
        throw ExceptionService.GenerateException(400, 'O campo história é obrigatório');
    }

    if(!priority) {
        throw ExceptionService.GenerateException(400, 'O campo prioridade é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const userIndex = projectBD.users.findIndex((item) => item.user.toString() === userId);
    const user = projectBD.users[userIndex];

    if(!user ||user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para editar a história');
    }

    const storyBD = await Story.findByIdAndUpdate(storyId, { 
        description,
        priority,
        updateAt: new Date(),
        updateBy: userId
    }, { new: true });

    return { story: storyBD };
}


export const FindById = async (id: string, userId: string) => {

    const storyBD = await Story.findById(id).limit(1);

    if(!storyBD) {
        throw ExceptionService.GenerateException(400, 'História não encontrada');
    }

    return { 
        _id: storyBD._id,
        description: storyBD.description,
        priority: storyBD.priority,
        sprintId: storyBD.sprint
    };
}


export const ListWithActivities = async (filter: any, userId: string) => {

    const filterStories = {
        sprint: filter.sprint
    };

    const stories = await Story
        .find(filterStories)
        .populate(['createdBy']);

    let storyById:any = {};
    let storiesId:string[] = [];

    stories.forEach(story => {
        storyById[story._id.toString()] = {
            story: {
                _id: story._id,
                count: story.count,
                description: story.description,
                priority: story.priority,
                createdByName: story.createdBy.name,
                sprintId: story.sprint
            },
            toDo: [],
            inProgress: [],
            toVerify: [],
            done: []
        };

        storiesId.push(story._id);
    });

    let filterActivities: any = { story: { $in: storiesId }};

    if(filter.name) {
        filterActivities.title = filter.name;
    }

    const activities = await Activity
        .find(filterActivities)
        .populate(['createdBy', 'responsible']);

    activities.forEach(activity => {
        storyById[activity.story.toString()][activity.status].push({
            _id: activity._id,
            count: activity.count,
            title: activity.title,
            priority: activity.priority,
            responsibleName: activity.responsible.name
        });
    });

    return Object.values(storyById);
}

export const Delete = async (id: string, userId: string) => {
    
    const storyBD = await Story.findOne({_id: id});

    if(!storyBD) {
        throw ExceptionService.GenerateException(400, 'História não encontrada');
    }

    const projectId = storyBD.project.toString();
    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const user = projectBD.users.find((item) => item.user.toString() === userId);

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para deletar a história');
    }

    const activities = await Activity.find({story: id});
    activities.forEach(async activity => {
        await activityRepository.decrementCountActivities(projectId, activity.status);
        activity.deleteOne();
    });

    storyBD.deleteOne();
}