import { IUserModel } from "./auth.models";


export interface IProjectModel { 
    name: string;
    description?: string;
    isFavorite: boolean;
    countActivities: number;
    countDone: number;
    countDoing: number;
    countVerify: number;
    users: IUserProjectModel[];
    createdAt: Date;
    createdBy: IUserModel;
};

export interface IUserProjectModel {
    user: IUserModel;
    permission: Permission;
}

export enum Permission {
    MANAGER = 'manager',
    DEFAULT = 'default'
}

export interface IListProjectModel {
    _id: string;
    name: string;
    description?: string;
    isFavorite: boolean;
    createdByName: string;
    countActivities: number;
    countDone: number;
    countDoing: number;
    countVerify: number;
}

export interface IChangeIsFavoriteModel {
    projectId: string;
    isFavorite: boolean;
}