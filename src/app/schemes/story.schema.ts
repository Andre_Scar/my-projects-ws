import { ObjectId } from 'mongodb';
import mongoose from '../database';
import { IStoryEntity, Priority } from '../entities/story.entity';


export const StorySchema = new mongoose.Schema<IStoryEntity>({
    count: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    priority: {
        type: Priority,
        required: true
    },
    sprint: {
        type: ObjectId,
        ref: 'Sprint',
        required: true
    },
    project: {
        type: ObjectId,
        ref: 'Project',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    createdBy: {
        type: ObjectId,
        ref: 'User',
        required: true
    }
});


const Story = mongoose.model<IStoryEntity>('Story', StorySchema);

export default Story;