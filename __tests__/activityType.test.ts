import request from "supertest";
import { ApiActivity } from "../.jest/api/activity.api";
import { ApiAuth } from "../.jest/api/auth.api";
import { ApiProject } from "../.jest/api/project.api";
import { users } from "../.jest/mocks/user.mock";
import { commonAuthHeaders, dropDatabase } from "../.jest/utils";
import { serve } from "../src/server";

const END_POINT: string = `/activity-type/v1`;

describe('Activity type', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {

        await dropDatabase();
        await serve.close();

    });

    beforeEach(async () => {

        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();

    });
    //#endregion

    test('Create activity type: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityType = {
            name: 'bug',
            description: 'Atividades realacionadas a bugs',
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityType)
            .set(commonAuthHeaders(tokenAdmin));
        
        const {name, description, project } = response.body.activityType;
        
        expect(name).toBe(activityType.name);
        expect(description).toBe(activityType.description);
        expect(project).toBe(activityType.projectId);
    
    });

    test('Create activity type no name: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityType = {
            name: null,
            description: 'Atividades realacionadas a bugs',
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityType)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    
    });

    test('Create activity type in fake project: Should fail', async () => {
    
        const fakeProjectId = '5fa0af509db40713507b0767';

        const activityType = {
            name: 'bug',
            description: 'Atividades realacionadas a bugs',
            projectId: fakeProjectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityType)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    
    });

    test('Create activity type by default user: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityType = {
            name: 'bug',
            description: 'Atividades realacionadas a bugs',
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(activityType)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para criar tipo de atividade');
    
    });

    test('Update activity type: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityType = {
            name: 'bug 1',
            description: 'Atividades realacionadas a bugs',
            projectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${activityTypeId}`)
            .send(activityType)
            .set(commonAuthHeaders(tokenAdmin));
        
        const {name, description, project } = response.body.activityType;
        
        expect(name).toBe(activityType.name);
        expect(description).toBe(activityType.description);
        expect(project).toBe(activityType.projectId);
    
    });

    test('Update activity type no name: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityType = {
            name: null,
            description: 'Atividades realacionadas a bugs',
            projectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${activityTypeId}`)
            .send(activityType)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo nome é obrigatório');
    
    });

    test('Update activity type in fake project: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const fakeProjectId = '5fa0af509db40713507b0767';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityType = {
            name: 'Bug 1',
            description: 'Atividades realacionadas a bugs',
            projectId: fakeProjectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${activityTypeId}`)
            .send(activityType)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    
    });

    test('Update activity type by default user: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';

        const activityType = {
            name: 'Bug 1',
            description: 'Atividades realacionadas a bugs',
            projectId
        };

        const response = await request(serve)
            .put(`${END_POINT}/${activityTypeId}`)
            .send(activityType)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para editar o tipo de atividade');
    
    });
    
    test('Find activity type by id: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';
        
        const response = await request(serve)
            .get(`${END_POINT}/${activityTypeId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { _id, name } = response.body;
        
        expect(_id).toBe(activityTypeId);
        expect(name).toBe('bug');

    });

    test('Find fake activity type by id: Should pass', async () => {

        const fakeActivityTypeId = '5fa0af509db40713507b0767';

        const response = await request(serve)
            .get(`${END_POINT}/${fakeActivityTypeId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Tipo de atividade não encontrada');
    });

    test('List activity type: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        for(var i = 0; i < 5; i++) {
            await ApiActivity.Type(`bug ${i}`, projectId, tokenAdmin);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({
                pageSize: 10,
                page: 0,
                filter: {project: projectId}
            })
            .set(commonAuthHeaders(tokenAdmin));

        const { count, results } = response.body;

        expect(count).toBe(5);
        expect(results.length).toBe(5);
        expect(results[0].name).toBe('bug 0');
        expect(results[1].name).toBe('bug 1');
        expect(results[2].name).toBe('bug 2');
        expect(results[3].name).toBe('bug 3');
        expect(results[4].name).toBe('bug 4');
    });

    test('List activity type filtered by name: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        for(var i = 0; i < 5; i++) {
            await ApiActivity.Type(`bug ${i}`, projectId, tokenAdmin);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({
                pageSize: 10,
                page: 0,
                filter: {
                    project: projectId,
                    name: {'$regex' : '1', '$options' : 'i'}
                }
            })
            .set(commonAuthHeaders(tokenAdmin));

        const { count, results } = response.body;

        expect(count).toBe(1);
        expect(results.length).toBe(1);
        expect(results[0].name).toBe('bug 1');

    });

    test('List all activity type: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        for(var i = 0; i < 5; i++) {
            await ApiActivity.Type(`bug ${i}`, projectId, tokenAdmin);
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list-all`)
            .send({ projectId })
            .set(commonAuthHeaders(tokenAdmin));

        expect(response.body.length).toBe(5);
        expect(response.body[0].name).toBe('bug 0');
        expect(response.body[1].name).toBe('bug 1');
        expect(response.body[2].name).toBe('bug 2');
        expect(response.body[3].name).toBe('bug 3');
        expect(response.body[4].name).toBe('bug 4');
    });

    test('Dalete activity type by user admin: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${activityTypeId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { status } = response;
        expect(status).toBe(200);

    });

    test('Dalete fake activity type: Should fail', async () => {

        const fakeSprintId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${fakeSprintId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Tipo de atividade não encontrado');

    });

    test('Dalete activity type by user default: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${activityTypeId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para deletar o tipo de atividade');
        
    });

    test('Dalete activity type with project not found: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const activityTypeDB = await ApiActivity.Type('bug', projectId, tokenAdmin);
        const activityTypeId = activityTypeDB ? activityTypeDB._id.toString() : '';
        
        await ApiProject.Delete(projectId);

        const response = await request(serve)
            .delete(`${END_POINT}/${activityTypeId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
        
    });
});