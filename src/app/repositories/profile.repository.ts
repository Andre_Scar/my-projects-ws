
import { IProfileModel } from "../models/profile.models";
import Project from "../schemes/project.schema";
import User from "../schemes/user.schema";
import { ExceptionService } from "../services/exception.service";


export const Update = async (profileModel: IProfileModel, userId: string)  => {

    const { 
        name
    } = profileModel;

    if(!name || name === '') {
        throw ExceptionService.GenerateException(400, 'O campo nome é obrigatório');
    }

    const userBD = await User.findByIdAndUpdate(userId, { 
        name,
    }, { new: true });

    return userBD;
}


export const FindById = async (userId: string) => {

    const userBD = await User.findById(userId).limit(1);

    if(!userBD) {
        throw ExceptionService.GenerateException(400, 'Usuário não encontrado');
    }


    const countMyProjects = await Project.find({
        createdBy: userId
    }).countDocuments();

    const countSharedWithMe = await Project.find({
        createdBy: {
            '$ne': userId
        }, 
        users: { 
            '$elemMatch': { 
                user: userId
            } 
        }
    }).countDocuments();

    return { 
        name: userBD.name,
        email: userBD.email,
        countMyProjects,
        countSharedWithMe,
    };
}