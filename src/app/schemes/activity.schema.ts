import { ObjectId } from 'mongodb';
import mongoose from '../database';
import { ActivityStatus, IActivityEntity } from '../entities/activity.entity';
import { Priority } from '../entities/story.entity';


export const ActivitySchema = new mongoose.Schema<IActivityEntity>({
    count: {
        type: Number,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    responsible: {
        type: ObjectId,
        ref: 'User',
        required: true
    },
    type: {
        type: ObjectId,
        ref: 'ActivityType',
        required: true
    },
    status: {
        type: ActivityStatus,
        required: true
    },
    priority: {
        type: Priority,
        required: true
    },
    story: {
        type: ObjectId,
        ref: 'Story',
        required: true
    },
    project: {
        type: ObjectId,
        ref: 'Project',
        required: true
    },
    customFields: [
        {
            activityCustomField: {
                type: ObjectId,
                ref: 'ActivityCustomField',
                required: true
            },
            valueText: {
                type: String
            },
            valueNumber: {
                type: Number
            },
            valueDate: {
                type: Date
            },
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now
    },
    createdBy: {
        type: ObjectId,
        ref: 'User',
        required: true
    }
});


const Activity = mongoose.model<IActivityEntity>('Activity', ActivitySchema);

export default Activity;