import { IUserModel } from "./auth.models";


export interface IActivityTypeModel {
    name: string;
    description?: string;
    projectId?: string;
    createdAt?: Date;
    createdBy: IUserModel;
    updateAt?: Date;
    updateBy?: IUserModel;
};

export interface IListActivityTypeModel {
    _id: string;
    name: string;
    description?: string;
    createdByName: string;
    projectId?: string;
}

export interface IFilterByProjectId {
    projectId: string;
}