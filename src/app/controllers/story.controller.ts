import * as storyRepository  from '../repositories/story.repository';
import { IRequest, IResponse } from '../types/express';


export const Save = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await storyRepository.Create(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Update = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await storyRepository.Update(request.body, request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const FindById = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await storyRepository.FindById(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const ListWithActivities = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await storyRepository.ListWithActivities(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Delete = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await storyRepository.Delete(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}
