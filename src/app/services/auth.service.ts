import jwt from 'jsonwebtoken';


export class AuthService {

    static GenerateToken(parmas = {}) {
        return jwt.sign(parmas, process.env.AUTH_SECRET || '', { expiresIn: process.env.APP_TOKEN_VALIDITY });
    }

    static InvalidToken(parmas = {}) {
        return jwt.sign(parmas, process.env.AUTH_SECRET || '', { expiresIn: 1 });
    }
}