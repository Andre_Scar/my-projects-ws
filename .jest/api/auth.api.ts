import { serve } from "../../src/server";
import request from "supertest";
import { commonHeaders } from "../utils";
import { IUserModel } from "../../src/app/models/auth.models";

const END_POINT: string = `/auth/v1`;

export class ApiAuth {

    static Create = async (userModel: IUserModel) => {
        const response = await request(serve)
            .post(`${END_POINT}/register`)
            .send(userModel)
            .set(commonHeaders());

        return response.body;
    }

}