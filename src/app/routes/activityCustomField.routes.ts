import express from 'express';
import authMiddleware from './../middlewares/auth';

import * as activityCustomFieldController from '../controllers/activityCustomField.controller';


const router = express.Router();

router.use(authMiddleware);

router.post('/', activityCustomFieldController.Save);
router.put('/:id', activityCustomFieldController.Update);
router.get('/:id', activityCustomFieldController.FindById);
router.post('/list', activityCustomFieldController.List);
router.post('/list-all', activityCustomFieldController.ListAll);
router.delete('/:id', activityCustomFieldController.Delete);


export default (app: any) => app.use('/activity-custom-field/v1', router);