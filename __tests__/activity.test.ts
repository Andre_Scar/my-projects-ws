import request from "supertest";
import { ApiActivity } from "../.jest/api/activity.api";
import { ApiAuth } from "../.jest/api/auth.api";
import { ApiProject } from "../.jest/api/project.api";
import { ApiSprint } from "../.jest/api/sprint.api";
import { ApiStory } from "../.jest/api/story.api";
import { users } from "../.jest/mocks/user.mock";
import { commonAuthHeaders, dropDatabase } from "../.jest/utils";
import { ActivityStatus } from "../src/app/entities/activity.entity";
import { Priority } from "../src/app/entities/story.entity";
import { serve } from "../src/server";

const END_POINT: string = `/activity/v1`;

describe('Activity', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {

        await dropDatabase();
        await serve.close();

    });

    beforeEach(async () => {

        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();

    });
    //#endregion

    test('Create activity: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activity = {
            title: 'activity 1',
            responsibleId: adminUserId,
            typeId: typeId,
            status: ActivityStatus.TO_DO,
            priority: Priority.HIGH,
            storyId,
            projectId
        };

        const response = await request(serve)
            .post(`/activity/v1`)
            .send(activity)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { title, responsible, type, status, story, project } = response.body.activity;
        
        expect(title).toBe('activity 1');
        expect(responsible).toBe(adminUserId);
        expect(type).toBe(typeId);
        expect(status).toBe(ActivityStatus.TO_DO);
        expect(story).toBe(storyId);
        expect(project).toBe(projectId);
    
    });

    test('Create activity no title: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activity = {
            title: null,
            responsibleId: adminUserId,
            typeId: typeId,
            status: ActivityStatus.TO_DO,
            priority: Priority.HIGH,
            storyId,
            projectId
        };

        const response = await request(serve)
            .post(`/activity/v1`)
            .send(activity)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo título é obrigatório');
    
    });

    test('Create activity no responsible: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activity = {
            title: 'Atividade 1',
            responsibleId: null,
            typeId: typeId,
            status: ActivityStatus.TO_DO,
            priority: Priority.HIGH,
            storyId,
            projectId
        };

        const response = await request(serve)
            .post(`/activity/v1`)
            .send(activity)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo responsável é obrigatório');
    
    });

    test('Create activity no type: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const activity = {
            title: 'Atividade 1',
            responsibleId: adminUserId,
            typeId: null,
            status: ActivityStatus.TO_DO,
            priority: Priority.HIGH,
            storyId,
            projectId
        };

        const response = await request(serve)
            .post(`/activity/v1`)
            .send(activity)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo tipo é obrigatório');
    
    });

    test('Create activity no status: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activity = {
            title: 'Atividade 1',
            responsibleId: adminUserId,
            typeId: typeId,
            status: null,
            priority: Priority.HIGH,
            storyId,
            projectId
        };

        const response = await request(serve)
            .post(`/activity/v1`)
            .send(activity)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo status é obrigatório');
    
    });

    test('Create activity no priority: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activity = {
            title: 'Atividade 1',
            responsibleId: adminUserId,
            typeId: typeId,
            status: ActivityStatus.TO_DO,
            priority: null,
            storyId,
            projectId
        };

        const response = await request(serve)
            .post(`/activity/v1`)
            .send(activity)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo prioridade é obrigatório');
    
    });

    test('Create activity in fake project: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const fakeProjectId = '5fa0af509db40713507b0767';

        const activity = {
            title: 'Atividade 1',
            responsibleId: adminUserId,
            typeId: typeId,
            status: ActivityStatus.TO_DO,
            priority: Priority.HIGH,
            storyId,
            projectId: fakeProjectId
        };

        const response = await request(serve)
            .post(`/activity/v1`)
            .send(activity)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    
    });

    test('Update activity: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activityDB = await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin);
        const activityId = activityDB ? activityDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${activityId}`)
            .send({
                title: 'Atividade 2',
                priority: Priority.MEDIUM,
                responsibleId: defaultUserId,
                status: ActivityStatus.DONE
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { title, priority, responsible, status } = response.body.activity;
        
        expect(title).toBe('Atividade 2');
        expect(priority).toBe(Priority.MEDIUM);
        expect(responsible).toBe(defaultUserId);
        expect(status).toBe(ActivityStatus.DONE);
    
    });

    test('Update activity no title: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activityDB = await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin);
        const activityId = activityDB ? activityDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${activityId}`)
            .send({
                title: null,
                priority: Priority.MEDIUM,
                responsibleId: defaultUserId,
                status: ActivityStatus.DONE
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo título é obrigatório');
    
    });

    test('Update activity no responsible: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activityDB = await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin);
        const activityId = activityDB ? activityDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${activityId}`)
            .send({
                title: 'Atividade 2',
                priority: Priority.MEDIUM,
                responsibleId: null,
                status: ActivityStatus.DONE
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo responsável é obrigatório');
    
    });

    test('Update activity no status: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activityDB = await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin);
        const activityId = activityDB ? activityDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${activityId}`)
            .send({
                title: 'Atividade 2',
                priority: Priority.MEDIUM,
                responsibleId: defaultUserId,
                status: null
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo status é obrigatório');
    
    });

    test('Update activity no priority: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        const activityDB = await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin);
        const activityId = activityDB ? activityDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${activityId}`)
            .send({
                title: 'Atividade 2',
                priority: null,
                responsibleId: defaultUserId,
                status: ActivityStatus.DONE
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo prioridade é obrigatório');
    
    });

    test('Update fake activity: Should fail', async () => {
    
        const fakeActivityId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .put(`${END_POINT}/${fakeActivityId}`)
            .send({
                title: 'Atividade 2',
                priority: Priority.MEDIUM,
                responsibleId: defaultUserId,
                status: ActivityStatus.DONE
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Atividade não encontrada');
    
    });

    test('Find activity by id: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeDBId = typeDB ? typeDB._id.toString() : '';

        const activityDB = await ApiActivity.Create('activity 1', adminUserId, typeDBId, projectId, storyId, tokenAdmin);
        const activityId = activityDB ? activityDB._id.toString() : '';
        
        const response = await request(serve)
            .get(`${END_POINT}/${activityId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { _id, count, title, status, priority, typeId, responsibleId } = response.body;
        
        expect(_id).toBe(activityId);
        expect(count).toBe(1);
        expect(title).toBe('activity 1');
        expect(status).toBe(ActivityStatus.TO_DO);
        expect(priority).toBe(Priority.HIGH);
        expect(typeId).toBe(typeDBId);
        expect(responsibleId).toBe(adminUserId);

    });

    test('Find fake activity: Should fail', async () => {
    
        const fakeActivityId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .get(`${END_POINT}/${fakeActivityId}`)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Atividade não encontrada');
    
    });

    test('Dalete activity by user admin: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeDBId = typeDB ? typeDB._id.toString() : '';

        const activityDB = await ApiActivity.Create('activity 1', adminUserId, typeDBId, projectId, storyId, tokenAdmin);
        const activityId = activityDB ? activityDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${activityId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { status } = response;
        expect(status).toBe(200);

    });

    test('Dalete fake activity: Should pass', async () => {

        const fakeActivityId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${fakeActivityId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Atividade não encontrada');

    });

    test('Dalete activity by user default: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeDBId = typeDB ? typeDB._id.toString() : '';

        const activityDB = await ApiActivity.Create('activity 1', adminUserId, typeDBId, projectId, storyId, tokenAdmin);
        const activityId = activityDB ? activityDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${activityId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para deletar a atividade');

    });

    test('Dalete activity with project not found: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeDBId = typeDB ? typeDB._id.toString() : '';

        const activityDB = await ApiActivity.Create('activity 1', adminUserId, typeDBId, projectId, storyId, tokenAdmin);
        const activityId = activityDB ? activityDB._id.toString() : '';
        
        await ApiProject.Delete(projectId);

        const response = await request(serve)
            .delete(`${END_POINT}/${activityId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
        
    });

});