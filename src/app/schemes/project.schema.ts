import { ObjectId } from 'mongodb';
import mongoose from '../database';
import { IProjectEntity } from '../entities/project.entity';


export const ProjectSchema = new mongoose.Schema<IProjectEntity>({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    countActivities: {
        type: Number,
        required: true
    },
    countDone: {
        type: Number,
        required: true
    },
    countDoing: {
        type: Number,
        required: true
    },
    countVerify: {
        type: Number,
        required: true
    },
    users: [
        {
            user: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },
            permission: {
                type: String,
                required: true
            },
            isFavorite: {
                type: Boolean,
                required: true
            }
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});


const Project = mongoose.model<IProjectEntity>('Project', ProjectSchema);

export default Project;