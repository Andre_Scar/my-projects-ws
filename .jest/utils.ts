import mongoose from "../src/app/database"

export const commonAuthHeaders = (token: string) => {
    return Object.assign( (token ? { 'authorization': `Bearer ${token}` } : {}), commonHeaders())
}

export const commonHeaders = () => {
    return {
        'Content-Type': 'application/json'
    }
}

export const dropDatabase = async () => {
    await mongoose.connect(process.env.MONGO_URL || '');
    await mongoose.connection.db.dropDatabase();
}

export const addDays = (date: Date, days: number) => {
    date.setDate(date.getDate() + days);
    return date;
}