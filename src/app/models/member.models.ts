import { Permission } from "./project.models";


export interface IMemberChangePermissionModel { 
    projectId: string;
    permission: Permission;
};

export interface IMemberListModel {
    _id: string;
    name: string;
    email: string;
    permission: Permission;
    isOwner: boolean;
}

export interface IMemberListFilter {
    projectId: string;
    name?: string;
}

export interface INewProjectMember {
    projectId: string;
    validity: string;
}