import { ActivityStatus, ICustomField } from "../entities/activity.entity";
import { IActivityModel } from "../models/activity.models";
import { Permission } from "../models/project.models";
import Activity from "../schemes/activity.schema";
import Project from "../schemes/project.schema";
import { ExceptionService } from "../services/exception.service";


export const Create = async (activityModel: IActivityModel, userId: string)  => {

    const { 
        title,
        description,
        responsibleId,
        typeId,
        status,
        priority,
        storyId,
        projectId
    } = activityModel;

    const customFields = activityModel.customFields || [];

    if(!title || title === '') {
        throw ExceptionService.GenerateException(400, 'O campo título é obrigatório');
    }

    if(!responsibleId || responsibleId === '') {
        throw ExceptionService.GenerateException(400, 'O campo responsável é obrigatório');
    }

    if(!typeId || typeId === '') {
        throw ExceptionService.GenerateException(400, 'O campo tipo é obrigatório');
    }

    if(!status) {
        throw ExceptionService.GenerateException(400, 'O campo status é obrigatório');
    }

    if(!priority) {
        throw ExceptionService.GenerateException(400, 'O campo prioridade é obrigatório');
    }

    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const lastActivityDB = await Activity.findOne({project: projectId}).sort({count: -1});

    const activityBD = await Activity.create({ 
        count: lastActivityDB ? lastActivityDB.count + 1 : 1,
        title,
        description,
        priority,
        status,
        responsible: responsibleId,
        type: typeId,
        story: storyId,
        project: projectId,
        createdBy: userId,
        customFields: customFields.map(e => {
            return {
                activityCustomField: e.activityCustomFieldId,
                valueText: e.valueText,
                valueNumber: e.valueNumber,
                valueDate: e.valueDate
            }
        })
    });

    await incrementCountActivities(projectId, status, null);

    return { activity: activityBD };
}


export const Update = async (activityModel: IActivityModel, activityId: string, userId: string)  => {

    const { 
        title,
        description,
        priority,
        responsibleId,
        status
    } = activityModel;

    const customFields = activityModel.customFields || [];

    if(!title || title === '') {
        throw ExceptionService.GenerateException(400, 'O campo título é obrigatório');
    }

    if(!responsibleId || responsibleId === '') {
        throw ExceptionService.GenerateException(400, 'O campo responsável é obrigatório');
    }

    if(!status) {
        throw ExceptionService.GenerateException(400, 'O campo status é obrigatório');
    }

    if(!priority) {
        throw ExceptionService.GenerateException(400, 'O campo prioridade é obrigatório');
    }

    const activityBD = await Activity.findById(activityId);

    if(!activityBD) {
        throw ExceptionService.GenerateException(400, 'Atividade não encontrada');
    }

    const result = await Activity.findByIdAndUpdate(activityId, { 
        title,
        description,
        priority,
        status,
        responsible: responsibleId,
        updateAt: new Date(),
        updateBy: userId,
        customFields: customFields.map(e => {
            return {
                activityCustomField: e.activityCustomFieldId,
                valueText: e.valueText,
                valueNumber: e.valueNumber,
                valueDate: e.valueDate
            }
        })
    }, { new: true });

    await incrementCountActivities(activityBD.project.toString(), status, activityBD.status);

    return { activity: result };
}


export const FindById = async (id: string, userId: string) => {

    const activityBD = await Activity.findById(id).limit(1).populate(['responsible']);

    if(!activityBD) {
        throw ExceptionService.GenerateException(400, 'Atividade não encontrada');
    }

    const customFields: any = {};

    const getValue = (value: ICustomField) => {
        if(value.valueDate != null) {
            return value.valueDate.toJSON().split('T')[0];
        }

        if(value.valueNumber != null) {
            return value.valueNumber
        }

        return value.valueText
    }

    activityBD.customFields.forEach(e => {
        customFields[e.activityCustomField] = getValue(e);
    });

    return { 
        _id: activityBD._id,
        count: activityBD.count,
        title: activityBD.title,
        status: activityBD.status,
        priority: activityBD.priority,
        typeId: activityBD.type,
        responsibleId: activityBD.responsible._id,
        description: activityBD.description,
        customFields: customFields
    };
}

export const Delete = async (id: string, userId: string) => {
    
    const activityBD = await Activity.findOne({_id: id});

    if(!activityBD) {
        throw ExceptionService.GenerateException(400, 'Atividade não encontrada');
    }

    const projectId = activityBD.project.toString();
    const projectBD = await Project.findOne({_id: projectId});

    if(!projectBD) {
        throw ExceptionService.GenerateException(400, 'Projeto não encontrado');
    }

    const user = projectBD.users.find((item) => item.user.toString() === userId);

    if(!user || user.permission !== Permission.MANAGER) {
        throw ExceptionService.GenerateException(400, 'Você não tem permissão para deletar a atividade');
    }

    await decrementCountActivities(projectId, activityBD.status);

    activityBD.deleteOne();
}

//#region help 
export const incrementCountActivities = async (projectId: string, currentStatus: ActivityStatus, oldStatus: ActivityStatus | null) => {
    if(currentStatus === oldStatus) {
        return null;
    }

    const incrementCounts = {
        countActivities: oldStatus === null ? 1 : 0,
        countDone: currentStatus === ActivityStatus.DONE ? 1 : oldStatus === ActivityStatus.DONE ? -1 : 0,
        countDoing: currentStatus === ActivityStatus.IN_PROGRESS ? 1 : oldStatus === ActivityStatus.IN_PROGRESS ? -1 : 0,
        countVerify: currentStatus === ActivityStatus.TO_VERIFY ? 1 : oldStatus === ActivityStatus.TO_VERIFY ? -1 : 0,
    };

    return await Project.findByIdAndUpdate(projectId, { 
        '$inc': {
            countActivities: incrementCounts.countActivities,
            countDone: incrementCounts.countDone,
            countDoing: incrementCounts.countDoing,
            countVerify: incrementCounts.countVerify
        }
    }, { new: true });
}

export const decrementCountActivities = async (projectId: string, currentStatus: ActivityStatus) => {

    const incrementCounts = {
        countActivities: -1,
        countDone: currentStatus === ActivityStatus.DONE ? -1 : 0,
        countDoing: currentStatus === ActivityStatus.IN_PROGRESS ? -1 : 0,
        countVerify: currentStatus === ActivityStatus.TO_VERIFY ? -1 : 0,
    };

    return await Project.findByIdAndUpdate(projectId, { 
        '$inc': {
            countActivities: incrementCounts.countActivities,
            countDone: incrementCounts.countDone,
            countDoing: incrementCounts.countDoing,
            countVerify: incrementCounts.countVerify
        }
    }, { new: true });
}
//#endregion