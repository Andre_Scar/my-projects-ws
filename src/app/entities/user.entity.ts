import mongoose from "../database";


export interface IUserEntity extends mongoose.Document { 
    name: string;
    email: string;
    password?: string;
    passwordResetToken?: string;
    passwordResetExpires?: Date;
    createdAt?: string;
};