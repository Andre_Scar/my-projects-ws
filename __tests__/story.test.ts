import request from "supertest";
import { ApiActivity } from "../.jest/api/activity.api";
import { ApiAuth } from "../.jest/api/auth.api";
import { ApiProject } from "../.jest/api/project.api";
import { ApiSprint } from "../.jest/api/sprint.api";
import { ApiStory } from "../.jest/api/story.api";
import { users } from "../.jest/mocks/user.mock";
import { commonAuthHeaders, dropDatabase } from "../.jest/utils";
import { CustomFieldType } from "../src/app/entities/activityCustomField.entity";
import { Priority } from "../src/app/entities/story.entity";
import { serve } from "../src/server";

const END_POINT: string = `/story/v1`;

describe('Story', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {

        await dropDatabase();
        await serve.close();

    });

    beforeEach(async () => {

        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();

    });
    //#endregion

    test('Create story: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const story = {
            description: 'Story 1',
            priority: Priority.MEDIUM,
            sprintId,
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(story)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { count, description, priority, sprint, project } = response.body.story;
        
        expect(count).toBe(1);
        expect(description).toBe(story.description);
        expect(priority).toBe(story.priority);
        expect(sprint).toBe(sprintId);
        expect(project).toBe(projectId);
    
    });

    test('Create story no description: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const story = {
            description: null,
            priority: Priority.MEDIUM,
            sprintId,
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(story)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo história é obrigatório');
    
    });

    test('Create story no priority: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const story = {
            description: 'Story 1',
            priority: null,
            sprintId,
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(story)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo prioridade é obrigatório');
    
    });

    test('Create story in fake project: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const fakeProjectId = '5fa0af509db40713507b0767';

        const story = {
            description: 'Story 1',
            priority: Priority.MEDIUM,
            sprintId,
            projectId: fakeProjectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(story)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    
    });

    test('Create story by default user: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const story = {
            description: 'Story 1',
            priority: Priority.MEDIUM,
            sprintId,
            projectId
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(story)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para cadastrar história');
    
    });

    test('Update story: Should pass', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${storyId}`)
            .send({
                description: 'Story 2',
                priority: Priority.LOW,
                projectId
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { description, priority } = response.body.story;
        
        expect(description).toBe('Story 2');
        expect(priority).toBe(Priority.LOW);
    
    });

    test('Update story no description: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${storyId}`)
            .send({
                description: null,
                priority: Priority.LOW,
                projectId
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo história é obrigatório');
    
    });

    test('Update story no priority: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${storyId}`)
            .send({
                description: 'Story 2',
                priority: null,
                projectId
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O campo prioridade é obrigatório');
    
    });

    test('Update story in fake project: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const fakeProjectId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .put(`${END_POINT}/${storyId}`)
            .send({
                description: 'Story 2',
                priority: Priority.LOW,
                projectId: fakeProjectId
            })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    
    });

    test('Update story by default user: Should fail', async () => {
    
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';
        
        const response = await request(serve)
            .put(`${END_POINT}/${storyId}`)
            .send({
                description: 'Story 2',
                priority: Priority.LOW,
                projectId
            })
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para editar a história');
    
    });

    test('Find story by id: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintDBId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintDBId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';
        
        const response = await request(serve)
            .get(`${END_POINT}/${storyId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { _id, description, priority, sprintId } = response.body;
        
        expect(_id).toBe(storyId);
        expect(description).toBe('Story 1');
        expect(priority).toBe(Priority.MEDIUM);
        expect(sprintId).toBe(sprintDBId);

    });

    test('Find fake story by id: Should pass', async () => {

        const fakeStoryId = '5fa0af509db40713507b0767';

        const response = await request(serve)
            .get(`${END_POINT}/${fakeStoryId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('História não encontrada');
    });

    test('List story: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        for(var i = 0; i < 5; i++) {
            await ApiStory.Create(`Story ${i}`, sprintId, projectId, tokenAdmin)
        }
        
        const response = await request(serve)
            .post(`${END_POINT}/list-with-activities`)
            .send({
                sprint: sprintId
            })
            .set(commonAuthHeaders(tokenAdmin));

        const results = response.body;

        expect(results.length).toBe(5);
        expect(results[0].story.description).toBe('Story 0');
        expect(results[1].story.description).toBe('Story 1');
        expect(results[2].story.description).toBe('Story 2');
        expect(results[3].story.description).toBe('Story 3');
        expect(results[4].story.description).toBe('Story 4');

    });

    test('List story with activity: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';

        const typeDB = await ApiActivity.Type('Type 1', projectId, tokenAdmin);
        const typeId = typeDB ? typeDB._id.toString() : '';

        await ApiActivity.Create('activity 1', adminUserId, typeId, projectId, storyId, tokenAdmin)
        
        const response = await request(serve)
            .post(`${END_POINT}/list-with-activities`)
            .send({
                sprint: sprintId
            })
            .set(commonAuthHeaders(tokenAdmin));

        const results = response.body;

        expect(results.length).toBe(1);
        expect(results[0].story.description).toBe('Story 1');
        expect(results[0].toDo[0].title).toBe('activity 1');

    });

    test('Dalete story by user admin: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${storyId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { status } = response;
        expect(status).toBe(200);

    });

    test('Dalete fake story: Should pass', async () => {

        const fakeStoryId = '5fa0af509db40713507b0767';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${fakeStoryId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('História não encontrada');

    });

    test('Dalete story by user default: Should pass', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';
        
        const response = await request(serve)
            .delete(`${END_POINT}/${storyId}`)
            .set(commonAuthHeaders(tokenDefault));

        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para deletar a história');

    });

    test('Dalete story with project not found: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const sprintDB = await ApiSprint.Create('Sprint 1', projectId, tokenAdmin);
        const sprintId = sprintDB ? sprintDB._id.toString() : '';

        const storyDB = await ApiStory.Create('Story 1', sprintId, projectId, tokenAdmin);
        const storyId = storyDB ? storyDB._id.toString() : '';
        
        await ApiProject.Delete(projectId);

        const response = await request(serve)
            .delete(`${END_POINT}/${storyId}`)
            .set(commonAuthHeaders(tokenAdmin));

        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
        
    });

});