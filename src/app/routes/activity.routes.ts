import express from 'express';
import authMiddleware from './../middlewares/auth';

import * as activityController from '../controllers/activity.controller';


const router = express.Router();

router.use(authMiddleware);

router.post('/', activityController.Save);
router.put('/:id', activityController.Update);
router.get('/:id', activityController.FindById);
router.delete('/:id', activityController.Delete);


export default (app: any) => app.use('/activity/v1', router);