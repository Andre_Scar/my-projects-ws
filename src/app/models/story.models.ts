import { Priority } from "../entities/story.entity";
import { IUserModel } from "./auth.models";


export interface IStoryModel { 
    count?: number;
    description: string;
    priority: Priority;
    sprintId: string;
    projectId: string;
    createdAt?: Date;
    createdBy: IUserModel;
};

export interface IListStoryModel {
    _id: string;
    count: number;
    description: string;
    priority: Priority;
    createdByName: string;
    sprintId: string;
}