import { serve } from "../../src/server";
import request from "supertest";
import { addDays, commonAuthHeaders } from "../../.jest/utils";

export class ApiSprint {

    static Create = async (name: string, projectId: string, tokenAdmin: string) => {

        const today = new Date();

        const sprint = {
            name: name,
            startAt: today,
            endAt: addDays(today, 15),
            projectId: projectId
        };

        const response = await request(serve)
            .post(`/sprint/v1/`)
            .send(sprint)
            .set(commonAuthHeaders(tokenAdmin));
        
        return response.body.sprint;

    }
    
}