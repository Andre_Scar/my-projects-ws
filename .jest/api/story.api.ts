import { serve } from "../../src/server";
import request from "supertest";
import { commonAuthHeaders } from "../../.jest/utils";
import { Priority } from "../../src/app/entities/story.entity";

export class ApiStory {

    static Create = async (description: string, sprintId: string, projectId: string, tokenAdmin: string, priority: Priority = Priority.MEDIUM) => {

        const story = {
            description,
            priority,
            sprintId,
            projectId
        };

        const response = await request(serve)
            .post(`/story/v1/`)
            .send(story)
            .set(commonAuthHeaders(tokenAdmin));
        
        return response.body.story;

    }
    
}