import * as dashboardRepository  from '../repositories/dashboard.repository';
import { IRequest, IResponse } from '../types/express';



export const TotalActivities = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await dashboardRepository.TotalActivities(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const TotalStoriesByPriority = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await dashboardRepository.TotalStoriesByPriority(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const TotalActivitiesBySprint = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await dashboardRepository.TotalActivitiesBySprint(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const MyActivitiesByStatusAndSprint = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await dashboardRepository.MyActivitiesByStatusAndSprint(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const MyActivitiesByPriorityAndSprint = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await dashboardRepository.MyActivitiesByPriorityAndSprint(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}

export const TotalActivitiesByPriorityAndSprint = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await dashboardRepository.TotalActivitiesByPriorityAndSprint(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}