import express from 'express';
import authMiddleware from './../middlewares/auth';

import * as memberController from '../controllers/member.controller';


const router = express.Router();

router.use(authMiddleware);

router.put('/:id', memberController.Update);
router.post('/list', memberController.List);
router.delete('/:id/:projectId', memberController.Delete);
router.get('/generate-link/:id', memberController.GenerateLink);
router.get('/projects-ingress/:code', memberController.Ingress);


export default (app: any) => app.use('/member/v1', router);