export interface IUserModel {
    name: string;
    email: string;
    password?: string;
    passwordResetToken?: string;
    passwordResetExpires?: Date;
    createdAt?: string;
};

export interface IResetPassword {
    email: string;
    token: string;
    password: string;
}

export interface IGoogleData {
    tokenId: string;
}