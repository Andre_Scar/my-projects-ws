import express from 'express';
import authMiddleware from './../middlewares/auth';

import * as projectController from '../controllers/project.controller';


const router = express.Router();

router.use(authMiddleware);

router.post('/', projectController.Save);
router.put('/:id', projectController.Update);
router.get('/:id', projectController.FindById);
router.post('/list', projectController.List);
router.delete('/:id', projectController.Delete);
router.post('/change-is-favorite', projectController.ChangeIsFavorite);


export default (app: any) => app.use('/project/v1', router);