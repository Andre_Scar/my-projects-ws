import mongoose from "../database";
import { IProjectEntity } from "./project.entity";
import { IUserEntity } from "./user.entity";


export interface ISprintEntity extends mongoose.Document { 
    name: string;
    startAt: Date;
    endAt: Date;
    project: IProjectEntity["_id"];
    createdAt?: Date;
    createdBy: IUserEntity["_id"];
};