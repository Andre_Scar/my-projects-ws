import authRoutes from "./auth.routes"
import projectRoutes from "./project.routes";
import sprintRoutes from "./sprint.routes"; 
import storyRoutes from "./story.routes"; 
import activityRoutes from "./activity.routes";
import memberRoutes from "./member.routes";
import activityTypeRoutes from "./activityType.routes";
import activityCustomFieldRoutes from "./activityCustomField.routes";
import profileRoutes from "./profile.routes";
import dashboardRoutes from "./dashboard.routes";

export default (app: any) => {
    authRoutes(app);
    projectRoutes(app);
    sprintRoutes(app);
    storyRoutes(app);
    activityRoutes(app);
    memberRoutes(app);
    activityTypeRoutes(app);
    activityCustomFieldRoutes(app);
    profileRoutes(app);
    dashboardRoutes(app);
} 