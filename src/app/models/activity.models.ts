import { ActivityStatus } from "../entities/activity.entity";
import { Priority } from "../entities/story.entity";
import { IUserModel } from "./auth.models";


export interface IActivityModel { 
    count?: number;
    title: string;
    description?: string;
    responsibleId: string;
    typeId: string;
    status: ActivityStatus;
    priority: Priority;
    storyId: string;
    projectId: string;
    customFields: ICustomFieldModel[];
    createdAt: Date;
    createdBy: IUserModel;
};

export interface IListActivityModel {
    _id: string;
    count: number;
    title: string;
    priority: Priority;
    responsibleName: string;
}

export interface ICustomFieldModel {
    activityCustomFieldId: string;
    valueText?: string | null;
    valueNumber?: number | null;
    valueDate?: Date | null;
}