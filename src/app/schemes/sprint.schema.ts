import { ObjectId } from 'mongodb';
import mongoose from '../database';
import { ISprintEntity } from '../entities/sprint.entity';


export const SprintSchema = new mongoose.Schema<ISprintEntity>({
    name: {
        type: String,
        required: true
    },
    startAt: {
        type: Date,
        required: true
    },
    endAt: {
        type: Date,
        required: true
    },
    project: {
        type: ObjectId,
        ref: 'Project',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    createdBy: {
        type: ObjectId,
        ref: 'User',
        required: true
    }
});


const Sprint = mongoose.model<ISprintEntity>('Sprint', SprintSchema);

export default Sprint;