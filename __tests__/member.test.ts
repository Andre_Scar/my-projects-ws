import request from "supertest";
import { ApiAuth } from "../.jest/api/auth.api";
import { commonAuthHeaders, dropDatabase } from "../.jest/utils";
import { serve } from "../src/server";
import { users } from "../.jest/mocks/user.mock";
import { ApiProject } from "../.jest/api/project.api";
import { Permission } from "../src/app/models/project.models";

const END_POINT: string = `/member/v1`;

describe('Members', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {

        await dropDatabase();
        await serve.close();

    });

    beforeEach(async () => {

        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();

    });

    test('Update member: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectDBId = projectDB ? projectDB.id.toString() : '';

        const changePermissio = {
            projectId: projectDBId,
            permission: Permission.MANAGER
        };

        const response = await request(serve)
            .put(`${END_POINT}/${defaultUserId}`)
            .send(changePermissio)
            .set(commonAuthHeaders(tokenAdmin));
        
        const {memberId, projectId, permission} = response.body;
        
        expect(memberId).toBe(defaultUserId);
        expect(projectId).toBe(projectDBId);
        expect(permission).toBe(Permission.MANAGER);
    });

    test('Update member with fake project: Should fail', async () => {
        
        const fakeProjectId = '5fa0af509db40713507b0767';

        const changePermissio = {
            projectId: fakeProjectId,
            permission: Permission.MANAGER
        };

        const response = await request(serve)
            .put(`${END_POINT}/${defaultUserId}`)
            .send(changePermissio)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');

    });

    test('Update member by default user: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectDBId = projectDB ? projectDB.id.toString() : '';

        const changePermissio = {
            projectId: projectDBId,
            permission: Permission.MANAGER
        };

        const response = await request(serve)
            .put(`${END_POINT}/${defaultUserId}`)
            .send(changePermissio)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para editar o membro');

    });

    test('Update fake member: Should fail', async () => {
        
        const fakeMemberId = '5fa0af509db40713507b0767';

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectDBId = projectDB ? projectDB.id.toString() : '';

        const changePermissio = {
            projectId: projectDBId,
            permission: Permission.MANAGER
        };

        const response = await request(serve)
            .put(`${END_POINT}/${fakeMemberId}`)
            .send(changePermissio)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Membro não encontrado');

    });

    test('Update project owner: Should fail', async () => {

        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectDBId = projectDB ? projectDB.id.toString() : '';

        const changePermissio = {
            projectId: projectDBId,
            permission: Permission.MANAGER
        };

        const response = await request(serve)
            .put(`${END_POINT}/${adminUserId}`)
            .send(changePermissio)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O membro é o dono do projeto');

    });

    test('List members: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({projectId})
            .set(commonAuthHeaders(tokenAdmin));
        
        const members = response.body;
        
        expect(members.length).toBe(2);
        expect(members[0]._id).toBe(adminUserId);
        expect(members[0].isOwner).toBe(true);
        expect(members[1]._id).toBe(defaultUserId);
        expect(members[1].isOwner).toBe(false);
    });

    test('List members by fake project: Should fail', async () => {
        
        const fakeProjectId = '5fa0af509db40713507b0767';

        const response = await request(serve)
            .post(`${END_POINT}/list`)
            .send({ projectId: fakeProjectId })
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    });

    test('Delete member: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        await request(serve)
            .delete(`${END_POINT}/${defaultUserId}/${projectId}`)
            .set(commonAuthHeaders(tokenAdmin));
        
        const response = await ApiProject.FindById(projectId);

        expect(response?.users.length).toBe(1);
        expect(response?.users[0].user).not.toBe(defaultUserId);
    });

    test('Delete member by fake project: Should fail', async () => {
        
        const fakeProjectId = '5fa0af509db40713507b0767';

        const response = await request(serve)
            .delete(`${END_POINT}/${defaultUserId}/${fakeProjectId}`)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');
    });

    test('Delete project owner: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const response = await request(serve)
            .delete(`${END_POINT}/${adminUserId}/${projectId}`)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('O membro é o dono do projeto');
    });

    test('Delete member by default user: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const response = await request(serve)
            .delete(`${END_POINT}/${defaultUserId}/${projectId}`)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para deletar o membro');
    });

    test('Generate share link: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const response = await request(serve)
            .get(`${END_POINT}/generate-link/${projectId}`)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { text } = response;
        expect(text).not.toBe(null);
        expect(text.length).not.toBe(0);

    });

    test('Generate fake project share link: Should fail', async () => {
        
        const fakeProjectId = '5fa0af509db40713507b0767';

        const response = await request(serve)
            .get(`${END_POINT}/generate-link/${fakeProjectId}`)
            .set(commonAuthHeaders(tokenAdmin));
        
        const { error } = response.body;
        expect(error).toBe('Projeto não encontrado');

    });

    test('Generate share link by default user: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const response = await request(serve)
            .get(`${END_POINT}/generate-link/${projectId}`)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você não tem permissão para compartilhar o projeto');

    });

    test('Ingress the project: Should pass', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const code = await ApiProject.GenerateShareLink(projectId, tokenAdmin);

        const user = await ApiAuth.Create({
            name: 'user 1',
            email: 'user@gmail.com',
            password: '123456',
        });

        await request(serve)
            .get(`${END_POINT}/projects-ingress/${code}`)
            .set(commonAuthHeaders(user.token));
        
        const response = await ApiProject.FindById(projectId);
        
        expect(response?.users.length).toBe(3);
        expect(response?.users[2].user.toString()).toBe(user.user._id.toString());

    });
    
    test('Ingress fake link: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const fakeCode = '1713683969fd7a42feb83709dc19cbb5';

        const user = await ApiAuth.Create({
            name: 'user 1',
            email: 'user@gmail.com',
            password: '123456',
        });

        const response = await request(serve)
            .get(`${END_POINT}/projects-ingress/${fakeCode}`)
            .set(commonAuthHeaders(user.token));
        
        const { error } = response.body;
        expect(error).toBe('Link inválido');
    });

    test('User is already inserted in the project: Should fail', async () => {
        
        const projectDB = await ApiProject.Create('Projeto 1', adminUserId, defaultUserId);
        const projectId = projectDB ? projectDB.id.toString() : '';

        const code = await ApiProject.GenerateShareLink(projectId, tokenAdmin);

        const response = await request(serve)
            .get(`${END_POINT}/projects-ingress/${code}`)
            .set(commonAuthHeaders(tokenDefault));
        
        const { error } = response.body;
        expect(error).toBe('Você ja é membro deste projeto');

    });

});