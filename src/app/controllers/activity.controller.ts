import * as activityRepository  from '../repositories/activity.repository';
import { IRequest, IResponse } from '../types/express';


export const Save = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityRepository.Create(request.body, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Update = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityRepository.Update(request.body, request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const FindById = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityRepository.FindById(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}


export const Delete = async (request: IRequest, response: IResponse) => {
    try {

        return response.send(await activityRepository.Delete(request.params.id, request.userId || ''));

    } catch (err) {

        return response.status(err.code || 400).send({ error: err.error });

    }
}
