import express from 'express';
import authMiddleware from './../middlewares/auth';

import * as profileController from '../controllers/profile.controller';


const router = express.Router();

router.use(authMiddleware);

router.put('/', profileController.Update);
router.get('/', profileController.FindById);


export default (app: any) => app.use('/profile/v1', router);