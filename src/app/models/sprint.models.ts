import { IUserModel } from "./auth.models";


export interface ISprintModel { 
    name: string;
    startAt: Date;
    endAt: Date;
    projectId: string;
    createdAt: Date;
    createdBy: IUserModel;
};

export interface IListSprintModel {
    _id: string;
    name: string;
    startAt: Date;
    endAt: Date;
    createdByName: string;
}

export interface IChangeIsFavoriteModel {
    sprintId: string;
    isFavorite: boolean;
}