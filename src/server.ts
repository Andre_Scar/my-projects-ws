import app from './app/app';

export const serve = app.listen(process.env.PORT || process.env.APP_PORT, () => {
    console.log(`Server started on ${process.env.APP_URL}`);
});