import request from "supertest";
import { serve } from "../src/server";
import { commonAuthHeaders, commonHeaders, dropDatabase } from "../.jest/utils";
import { ApiAuth } from "../.jest/api/auth.api";
import User from "../src/app/schemes/user.schema";
import { users } from "../.jest/mocks/user.mock";

const END_POINT: string = `/project/v1`;

describe('Middlewares user', () => {

    let tokenAdmin: string = '';
    let tokenDefault: string = '';

    let adminUserId: string = '';
    let defaultUserId: string = '';

    //#region setup
    afterAll(async () => {
        await dropDatabase();
        await serve.close();
    });

    beforeEach(async () => {
        await dropDatabase();

        const adminUser = await ApiAuth.Create(users.admin);
        tokenAdmin = adminUser.token;
        adminUserId = adminUser.user._id.toString();

        const defaultUser = await ApiAuth.Create(users.default);
        tokenDefault = defaultUser.token;
        defaultUserId = defaultUser.user._id.toString();
    });
    //#endregion

    test('No tokens provided: Should fail', async () => {
        const project = {
            name: 'Projeto 1',
            description: 'teste',
            isFavorite: true
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(project)
            .set(commonHeaders());
        
        const { error } = response.body;
        expect(error).toBe('Nenhum token fornecido');

    });

    test('Token error: Should fail', async () => {
        
        const project = {
            name: 'Projeto 1',
            description: 'teste',
            isFavorite: true
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(project)
            .set({ 
                'authorization': tokenDefault,
                'Content-Type': 'application/json' 
            });
        
        const { error } = response.body;
        expect(error).toBe('Erro de token');

    });

    test('Invalid token: Should fail', async () => {
        
        const project = {
            name: 'Projeto 1',
            description: 'teste',
            isFavorite: true
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(project)
            .set(commonAuthHeaders('352090599931769174fd12fc4dba1660'));
        
        const { error } = response.body;
        expect(error).toBe('Token inválido');

    });

    test('Badly formatted token: Should fail', async () => {
        
        const project = {
            name: 'Projeto 1',
            description: 'teste',
            isFavorite: true
        };

        const response = await request(serve)
            .post(`${END_POINT}/`)
            .send(project)
            .set({ 
                'authorization': `Beare ${tokenAdmin}`,
                'Content-Type': 'application/json' 
            });
        
        const { error } = response.body;
        expect(error).toBe('Token mal formatado');

    });

});