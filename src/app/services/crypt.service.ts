import crypto from 'crypto';


export class CryptService {

    private static secret = process.env.CRYPT_SECRET || '';
    private static iv = process.env.CRYPT_IV || '';
    
    static Encrypt(text: string) {

        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(this.secret), this.iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);

        return encrypted.toString('hex');
    }

    static Decrypt(text: string) {
        try {

            let encryptedText = Buffer.from(text, 'hex');
            let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(this.secret), this.iv);
            let decrypted = decipher.update(encryptedText);
            decrypted = Buffer.concat([decrypted, decipher.final()]);
            return decrypted.toString();
    
        } catch (err) {
    
            return null;
    
        }
        
    }
}