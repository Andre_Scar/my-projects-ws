import mongoose from "../database";
import { IActivityTypeEntity } from "./activityType.entity";
import { IProjectEntity } from "./project.entity";
import { IUserEntity } from "./user.entity";


export interface IActivityCustomFieldEntity extends mongoose.Document { 
    name: string;
    type: CustomFieldType;
    activityType: IActivityTypeEntity["_id"];
    project: IProjectEntity["_id"];
    createdAt?: Date;
    createdBy: IUserEntity["_id"];
};


export enum CustomFieldType {
    DATE = 'date',
    TEXT = 'text',
    NUMBER = 'number'
}