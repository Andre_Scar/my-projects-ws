import { ObjectId } from 'mongodb';
import mongoose from '../database';
import { IActivityTypeEntity } from '../entities/activityType.entity';


export const ActivityTypeSchema = new mongoose.Schema<IActivityTypeEntity>({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    project: {
        type: ObjectId,
        ref: 'Project'
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    createdBy: {
        type: ObjectId,
        ref: 'User',
        required: true
    }
});


const ActivityType = mongoose.model<IActivityTypeEntity>('ActivityType', ActivityTypeSchema);

export default ActivityType;